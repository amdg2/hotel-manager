package hotelManager.view.component;

import hotelManager.controllers.WelcomeController;
import hotelManager.model.Hotel;

/**
 * Hotel list
 */
public class HotelList extends AbstractList<Hotel> {

    private WelcomeController controller;

    public HotelList(WelcomeController controller) {
        super();
        this.controller = controller;
    }

    protected void onItemAdd(HotelListItem hotel) {
        hotel.setController(controller);
    }

    @Override
    protected void onAddItem(AbstractListItem<Hotel> item) {
        if (item instanceof HotelListItem)
            onItemAdd((HotelListItem) item);
    }
}
