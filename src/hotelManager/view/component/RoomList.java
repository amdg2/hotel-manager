package hotelManager.view.component;

import hotelManager.controllers.NewBookingController;
import hotelManager.model.Room;

/**
 * Room List component
 */
public class RoomList extends AbstractList<Room> {

    private NewBookingController controller;

    public RoomList(NewBookingController controller) {
        super();
        this.controller = controller;
    }

    @Override
    protected void onAddItem(AbstractListItem<Room> item) {
        RoomListItem roomListItem = (RoomListItem) item;
        roomListItem.setController(controller);
    }
}
