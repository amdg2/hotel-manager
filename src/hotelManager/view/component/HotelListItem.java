package hotelManager.view.component;

import hotelManager.AppConfig;
import hotelManager.controllers.WelcomeController;
import hotelManager.model.Hotel;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

/**
 * Hotel List Item
 */
public class HotelListItem extends AbstractListItem<Hotel> {

    private static final int MIN_HOTEL_IMG_IDX = 1;
    private static final int MAX_HOTEL_IMG_IDX = 8;
    private JLabel hotelName;
    private JLabel hotelPhoto;

    private Hotel hotel;
    private WelcomeController controller;

    public HotelListItem(Hotel hotel) {
        super();
        this.hotel = hotel;
        initRandomHotelImage();
        initHotelNameLabel(hotel.getName());
    }

    public HotelListItem(String hotelNameStr) {
        super();
        initRandomHotelImage();
        initHotelNameLabel(hotelNameStr);
    }

    public HotelListItem(String hotelNameStr, String hotelImage) {
        super();
        initHotelImage(hotelImage);
        initHotelNameLabel(hotelNameStr);
    }

    private void initRandomHotelImage() {
        Random random = new Random();
        int hotelImgIdx = random.nextInt((MAX_HOTEL_IMG_IDX - MIN_HOTEL_IMG_IDX) + 1) + MIN_HOTEL_IMG_IDX;

        String imagePath = String.format("%s/%d.jpg", AppConfig.getHotelPhotoPath(), hotelImgIdx);

        hotelPhoto = new JLabel(new ImageIcon(imagePath));
        add(hotelPhoto, BorderLayout.WEST);
    }

    private void initHotelImage(String hotelImagePath) {
        String imagePath = String.format("%s/%s.jpg", AppConfig.getHotelPhotoPath(), hotelImagePath);

        hotelPhoto = new JLabel(new ImageIcon(imagePath));
        add(hotelPhoto, BorderLayout.WEST);
    }

    private void initHotelNameLabel(String hotelNameStr) {
        hotelName = new JLabel(hotelNameStr);
        hotelName.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 15));
        hotelName.setHorizontalAlignment(SwingConstants.CENTER);
        add(hotelName, BorderLayout.CENTER);
    }

    @Override
    public void onClick() {
        super.onClick();

        if (hotel != null)
            controller.openHotel(hotel);
        else
            controller.getAppController().showError("Can't open hotel", "Hotel doesn't have a correct database backend. Abort.");
    }

    public String getHotelName() {
        return hotelName.getText();
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public WelcomeController getController() {
        return controller;
    }

    public void setController(WelcomeController controller) {
        this.controller = controller;
    }

    @Override
    public boolean match(String filter) {
        return getHotelName().toLowerCase().contains(filter);
    }
}
