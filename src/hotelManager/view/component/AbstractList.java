package hotelManager.view.component;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Abstract item list
 */
public abstract class AbstractList<ItemClass> extends JScrollPane {

    private JPanel panel;

    private String filter = "";

    private ArrayList<AbstractListItem<ItemClass>> items = new ArrayList<>();

    public AbstractList() {
        setBorder(BorderFactory.createEmptyBorder());
        getVerticalScrollBar().setUnitIncrement(10);
        panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        getViewport().add(panel);
    }

    public void add(String hotelName, String hotelImage) {
        add(new HotelListItem(hotelName, hotelImage));
    }

    public void add(String hotelName) {
        add(new HotelListItem(hotelName));
    }

    public void add(AbstractListItem<ItemClass> item) {
        if (item == null)
            throw new NullPointerException("item parameter is null");

        items.add(item);
        panel.add(item);
        onAddItem(item);
    }

    protected abstract void onAddItem(AbstractListItem<ItemClass> item);

    public void setFilter(String text) {
        text = text.trim();
        if (!Objects.equals(text, filter)) {
            filter = text;
            filterItems();
        }
    }

    public void clear() {
        items.clear();
        panel.removeAll();
    }

    private void filterItems() {
        // Remove all child
        panel.removeAll();

        // Add only matching items
        if (filter != null && !Objects.equals(filter, "")) {
            items.stream().filter(i -> i.match(filter.toLowerCase())).forEach(h -> panel.add(h));
        } else {
            items.stream().forEach(i -> panel.add(i));
        }

        updateUI();
    }
}
