package hotelManager.view.component;

import hotelManager.controllers.NewBookingController;
import hotelManager.model.Room;

import javax.swing.*;
import java.awt.*;

/**
 * Room list item
 */
public class RoomListItem extends AbstractListItem<Room> {

    private NewBookingController controller;

    private Room room;

    public RoomListItem(Room room) {
        super();
        this.room = room;
        initContent();
    }

    private void initContent() {
        add(new JLabel(room.toString()), BorderLayout.WEST);
        add(new JLabel(getNbrBedsString()), BorderLayout.CENTER);
        add(new JLabel(String.format("%f €", room.getPrice())), BorderLayout.EAST);
    }

    private String getNbrBedsString() {
        return String.format("Lits simple: %d - Lits double: %d", room.getNbSimpleBed(), room.getNbDoubleBed());
    }

    @Override
    public void onClick() {
        controller.selectRoom(room);
    }

    @Override
    public boolean match(String s) {
        return true;
    }

    public Room getRoom() {
        return room;
    }

    public void setController(NewBookingController controller) {
        this.controller = controller;
    }
}
