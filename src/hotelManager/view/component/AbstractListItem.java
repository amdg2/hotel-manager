package hotelManager.view.component;

import hotelManager.controllers.MouseClickController;
import hotelManager.controllers.MouseHoverController;
import hotelManager.events.IMouseClickEventReceiver;
import hotelManager.events.IMouseHoverEventReceiver;
import hotelManager.utils.CustomPanel;

import javax.swing.*;
import java.awt.*;

/**
 * Abstract list item
 */
public abstract class AbstractListItem<ItemClass> extends CustomPanel implements IMouseHoverEventReceiver, IMouseClickEventReceiver {

    public AbstractListItem() {
        initPanel();
    }

    private void initPanel() {
        setLayout(new BorderLayout());
        setUseShadow(true);
        setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(5, 5, 5, 5),
                BorderFactory.createLineBorder(new Color(200, 200, 200))
        ));
        addMouseListener(new MouseClickController(this));
        addMouseListener(new MouseHoverController(this));
    }

    @Override
    public void OnMouseEnter() {
        showDropShadow();
        this.setCursor(new Cursor(Cursor.HAND_CURSOR));
    }

    @Override
    public void OnMouseLeave() {
        hideDropShadow();
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }

    @Override
    public void OnMouseHover(boolean hover) {

    }

    @Override
    public void onClick() {
        hideDropShadow();
    }

    @Override
    public void onRightClick() {

    }

    public abstract boolean match(String s);
}
