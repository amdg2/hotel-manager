package hotelManager.view;

import hotelManager.controllers.AppController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * MainWindow class
 *
 * @author Baudouin Feildel baudouin@feildel.org
 */
public class MainWindow extends JFrame {

    /**
     * Default height for MainWindow
     */
    private static final int WINDOW_WIDTH = 1024;

    /**
     * Default width for MainWindow
     */
    private static final int WINDOW_HEIGHT = 768;

    /**
     * Minimum width
     */
    private static final int WINDOW_MIN_WIDTH = 800;

    /**
     * Minimum height
     */
    private static final int WINDOW_MIN_HEIGHT = 600;

    private final AppController appController;

    /**
     * Left panel with menu
     */
    private JPanel leftPanel;

    /**
     * Main content panel
     */
    private JPanel mainPanel;

    public MainWindow(AppController app) {
        super("Hotel Manager");
        appController = app;
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setLayout(new BorderLayout());
        setMinimumSize(new Dimension(WINDOW_MIN_WIDTH, WINDOW_MIN_HEIGHT));
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - WINDOW_WIDTH / 2, dim.height / 2 - WINDOW_HEIGHT / 2);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {
                appController.onExitApplication();
                super.windowClosing(windowEvent);
            }
        });
    }

    private void refreshView() {
        repaint();
    }

    /**
     * Set the current main panel.
     * The window will be refreshed automatically.
     *
     * @param panel New panel to use
     */
    public void setMainPanel(JPanel panel) {
        setMainPanel(panel, true);
    }

    /**
     * Set the current main panel
     *
     * @param panel   New panel to use
     * @param refresh refresh the window
     */
    public void setMainPanel(JPanel panel, boolean refresh) {

        if (mainPanel != null)
            remove(mainPanel);

        this.mainPanel = panel;

        if (mainPanel != null)
            add(mainPanel, BorderLayout.CENTER);

        if(mainPanel != null)
            mainPanel.updateUI();

        if (refresh)
            refreshView();
    }

    /**
     * Set the current left panel.
     * The window will be refreshed automatically.
     *
     * @param panel New panel to use
     */
    public void setLeftPanel(JPanel panel) {
        setLeftPanel(panel, true);
    }

    /**
     * Set left panel
     *
     * @param panel   panel to use
     * @param refresh refresh the window
     */
    public void setLeftPanel(JPanel panel, boolean refresh) {
        if (leftPanel != null)
            remove(leftPanel);

        this.leftPanel = panel;

        if (leftPanel != null)
            add(leftPanel, BorderLayout.WEST);

        if(leftPanel != null)
            leftPanel.updateUI();

        if (refresh)
            refreshView();
    }
}
