package hotelManager.view;

import hotelManager.model.DbManager;
import hotelManager.model.SqlConnection;

import javax.swing.*;
import java.awt.*;
import java.sql.SQLException;

/**
 * Loading Window class
 */
public class LoadingWindow extends JFrame {

    private static final int WINDOW_WIDTH = 300;
    private static final int WINDOW_HEIGHT = 100;

    JProgressBar progressBar;

    public LoadingWindow() {
        super();
        setLayout(new BorderLayout());
        progressBar = new JProgressBar();
        add(progressBar, BorderLayout.CENTER);
        setPreferredSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        setMinimumSize(new Dimension(WINDOW_WIDTH, WINDOW_HEIGHT));
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
        setTitle("Connexion à la base de donnée");
        setVisible(true);
    }

    public boolean connectToDatabase() {
        progressBar.setIndeterminate(true);
        try {
            DbManager.getInstance();
            return true;
        }catch (SQLException e) {
            e.printStackTrace();
            return false;
        }catch (ClassNotFoundException cnfe){
            cnfe.printStackTrace();
            return false;
        } catch (Exception ex) {

            JOptionPane.showMessageDialog(null, "<html><p style='text-align: center'>Impossible de se connecter à la base de donnée.<br>Veuillez vérifier votre connexion réseau.</p></html>", "Erreur de connexion", JOptionPane.ERROR_MESSAGE);
            return false;
        }
    }
}
