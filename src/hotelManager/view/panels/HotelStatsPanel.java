package hotelManager.view.panels;

import hotelManager.controllers.HotelStatsController;
import hotelManager.model.Hotel;

import javax.swing.*;
import java.awt.*;

/**
 * Hotel statistics panel
 */
public class HotelStatsPanel extends JPanel {


    private static final String STAT_GRAPH = "Stat graph";
    private static final String STAT_TABLE = "Stat table";

    private Hotel hotel = null;

    private HotelStatsController controller;


    public HotelStatsPanel(HotelStatsController controller) {
        super();
        this.controller = controller;
        setLayout(new BorderLayout());
        initPanelHeader();
        initPanelContent();

    }

    private void initPanelContent() {

    }

    private void initPanelHeader() {

        JPanel headerPanel = new JPanel(new BorderLayout());
        JLabel panelTitle = new JLabel("Statistique de l'hotel");

        panelTitle.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 40));
        panelTitle.setHorizontalAlignment(SwingConstants.CENTER);

        headerPanel.add(panelTitle, BorderLayout.CENTER);
        add(headerPanel, BorderLayout.NORTH);
    }
}
