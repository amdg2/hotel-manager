package hotelManager.view.panels;

import hotelManager.Icons;
import hotelManager.controllers.CustomerEditorController;
import hotelManager.controllers.CustomerManagerController;
import hotelManager.model.Customer;
import hotelManager.model.Hotel;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import java.awt.*;

/**
 * Customer manager panel
 *
 * @author Kevin
 */
public class CustomerManagerPanel extends JPanel{
    private static final String CUSTOMER_EDITOR = "Customer Editor";
    private static final String CUSTOMER_MANAGER_HOME = "Customer Manager Home";

    private JList<Customer> customerList;

    private Hotel hotel = null;

    private CustomerManagerController controller;
    private CustomerEditorController customerEditor;
    private JPanel contentSwitcher;
    private JPanel managerHome;
    private CardLayout contentSwitcherLayout;

    public CustomerManagerPanel(CustomerManagerController controller) {
        super();
        this.controller = controller;
        setLayout(new BorderLayout());
        initPanelHeader();
        initPanelContent();
    }

    private void initPanelHeader() {
        JPanel headerPanel = new JPanel(new BorderLayout());
        JLabel panelTitle = new JLabel("Gestion des clients");

        panelTitle.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 40));
        panelTitle.setHorizontalAlignment(SwingConstants.CENTER);

        headerPanel.add(panelTitle, BorderLayout.CENTER);
        add(headerPanel, BorderLayout.NORTH);
    }

    private void initPanelContent() {
        customerList = new JList<>();
        customerList.setCellRenderer(new DefaultListCellRenderer() {
            @Override
            public Component getListCellRendererComponent(JList<?> jList, Object o, int i, boolean b, boolean b1) {
                Component c = super.getListCellRendererComponent(jList, o, i, b, b1);
                if(o instanceof Customer)
                    setText(((Customer)o).getName());
                return c;
            }
        });
        customerList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        customerList.addListSelectionListener(this::customerSelected);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setPreferredSize(new Dimension(200, 200));
        scrollPane.setViewportView(customerList);
        add(scrollPane, BorderLayout.WEST);

        contentSwitcher = new JPanel(new CardLayout());
        contentSwitcherLayout = (CardLayout) contentSwitcher.getLayout();

        customerEditor = new CustomerEditorController(controller);
        initManagerHome();

        contentSwitcher.add(customerEditor.getPanel(), CUSTOMER_EDITOR);
        contentSwitcher.add(managerHome, CUSTOMER_MANAGER_HOME);

        add(contentSwitcher, BorderLayout.CENTER);
        contentSwitcherLayout.show(contentSwitcher, CUSTOMER_MANAGER_HOME);
    }

    private void customerSelected(ListSelectionEvent event) {
        if (!customerList.isSelectionEmpty()) {
            selectCustomer(customerList.getSelectedValue());
        }
    }

    public void selectCustomer(Customer e) {
        if (e != null) {
            customerEditor.setCustomer(e);
            showCustomerEditor();
        }
    }

    private void initManagerHome() {
        managerHome = new JPanel(new BorderLayout());

        JLabel text = new JLabel("<html><div style='text-align: center;'>Veuillez sélectionner un client pour voir et éditer ses informations.<br>Ou utilisez le bouton ci-dessous pour ajouter un client.</div></html>");
        text.setVerticalTextPosition(JLabel.CENTER);
        text.setHorizontalAlignment(JLabel.CENTER);
        managerHome.add(text, BorderLayout.CENTER);

        JButton button = new JButton("Ajouter un employé", new ImageIcon(Icons.getPersonAddIcon()));
        button.addActionListener(actionEvent -> showNewCustomerEditor());
        managerHome.add(button, BorderLayout.SOUTH);
    }

    private void updateData() {
        showPanel(CUSTOMER_MANAGER_HOME);
    }

    public void setCustomerList(Customer[] customers) {
        customerList.setListData(customers);
    }

    public CustomerEditorController getEditorController() {
        return customerEditor;
    }

    public void showManagerHome() {
        showPanel(CUSTOMER_MANAGER_HOME);
    }

    public void showCustomerEditor() {
        customerEditor.openEditCustomerEditor();
        showPanel(CUSTOMER_EDITOR);
    }

    public void showNewCustomerEditor() {
        customerEditor.openNewCustomerEditor();
        showPanel(CUSTOMER_EDITOR);
    }

    private void showPanel(String panelName) {
        contentSwitcherLayout.show(contentSwitcher, panelName);
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        if (hotel == this.hotel)
            return;

        this.hotel = hotel;
        updateData();
    }

    public JList<Customer> getListCustomer() {
        return customerList;
    }
}
