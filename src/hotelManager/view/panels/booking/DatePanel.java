package hotelManager.view.panels.booking;

import hotelManager.model.DbManager;
import hotelManager.model.Hotel;
import hotelManager.utils.CustomDatePicker;
import hotelManager.utils.CustomSpinner;

import javax.swing.*;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

/**
 * New resa date panel
 */
public class DatePanel extends JPanel {

    private static final int PADDING = 15;
    private static final int LABEL_HEIGHT = 30;
    private static final int LINE_SEPARATION = 35;
    private static final int INPUT_WIDTH = 300;

    private JComboBox<Hotel> hotelJComboBox;
    private CustomDatePicker startDatePicker;
    private CustomDatePicker endDatePicker;
    private JLabel totalNightsLabel;
    private CustomSpinner bedNumberSpinner;

    private SpringLayout layout;

    public DatePanel() {
        super(new SpringLayout());
        layout = (SpringLayout) getLayout();

        initLabels();
        initInputs();
        initHotelList();
    }

    private void initHotelList() {
        try {
            List<Hotel> hotels = DbManager.getInstance().getListHotel();
            for (Hotel h : hotels) {
                hotelJComboBox.addItem(h);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initInputs() {
        hotelJComboBox = new JComboBox<>();
        startDatePicker = new CustomDatePicker();
        endDatePicker = new CustomDatePicker();
        totalNightsLabel = new JLabel("0");
        bedNumberSpinner = new CustomSpinner(new SpinnerNumberModel());

        startDatePicker.addActionListener(actionEvent -> updateTotalNights());
        endDatePicker.addActionListener(actionEvent -> updateTotalNights());

        add(hotelJComboBox);
        add(startDatePicker);
        add(endDatePicker);
        add(totalNightsLabel);
        add(bedNumberSpinner);

        layout.putConstraint(SpringLayout.EAST, hotelJComboBox, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, startDatePicker, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, endDatePicker, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, totalNightsLabel, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, bedNumberSpinner, -PADDING, SpringLayout.EAST, this);

        layout.putConstraint(SpringLayout.NORTH, hotelJComboBox, PADDING, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.NORTH, startDatePicker, LINE_SEPARATION, SpringLayout.NORTH, hotelJComboBox);
        layout.putConstraint(SpringLayout.NORTH, endDatePicker, LINE_SEPARATION, SpringLayout.NORTH, startDatePicker);
        layout.putConstraint(SpringLayout.NORTH, totalNightsLabel, LINE_SEPARATION, SpringLayout.NORTH, endDatePicker);
        layout.putConstraint(SpringLayout.NORTH, bedNumberSpinner, LINE_SEPARATION, SpringLayout.NORTH, totalNightsLabel);

        layout.putConstraint(SpringLayout.WEST, hotelJComboBox, -INPUT_WIDTH, SpringLayout.EAST, hotelJComboBox);
        layout.putConstraint(SpringLayout.WEST, startDatePicker, -INPUT_WIDTH, SpringLayout.EAST, startDatePicker);
        layout.putConstraint(SpringLayout.WEST, endDatePicker, -INPUT_WIDTH, SpringLayout.EAST, endDatePicker);
        layout.putConstraint(SpringLayout.WEST, bedNumberSpinner, -INPUT_WIDTH, SpringLayout.EAST, bedNumberSpinner);

        totalNightsLabel.setVerticalTextPosition(JLabel.CENTER);
        layout.putConstraint(SpringLayout.SOUTH, totalNightsLabel, LABEL_HEIGHT, SpringLayout.NORTH, totalNightsLabel);
    }

    private void initLabels() {
        JLabel hotelLabel = new JLabel("Hotel");
        JLabel startDateLabel = new JLabel("Date début");
        JLabel endDateLabel = new JLabel("Date fin");
        JLabel nightNumberLabel = new JLabel("Nombre de nuit(s)");
        JLabel bedNumberLabel = new JLabel("Nombre de lit(s)");

        hotelLabel.setVerticalTextPosition(JLabel.CENTER);
        startDateLabel.setVerticalTextPosition(JLabel.CENTER);
        endDateLabel.setVerticalTextPosition(JLabel.CENTER);
        nightNumberLabel.setVerticalTextPosition(JLabel.CENTER);
        bedNumberLabel.setVerticalTextPosition(JLabel.CENTER);

        add(hotelLabel);
        add(startDateLabel);
        add(endDateLabel);
        add(nightNumberLabel);
        add(bedNumberLabel);

        layout.putConstraint(SpringLayout.WEST, hotelLabel, PADDING, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, startDateLabel, PADDING, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, endDateLabel, PADDING, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, nightNumberLabel, PADDING, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, bedNumberLabel, PADDING, SpringLayout.WEST, this);

        layout.putConstraint(SpringLayout.NORTH, hotelLabel, PADDING, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.NORTH, startDateLabel, LINE_SEPARATION, SpringLayout.NORTH, hotelLabel);
        layout.putConstraint(SpringLayout.NORTH, endDateLabel, LINE_SEPARATION, SpringLayout.NORTH, startDateLabel);
        layout.putConstraint(SpringLayout.NORTH, nightNumberLabel, LINE_SEPARATION, SpringLayout.NORTH, endDateLabel);
        layout.putConstraint(SpringLayout.NORTH, bedNumberLabel, LINE_SEPARATION, SpringLayout.NORTH, nightNumberLabel);

        layout.putConstraint(SpringLayout.SOUTH, hotelLabel, LABEL_HEIGHT, SpringLayout.NORTH, hotelLabel);
        layout.putConstraint(SpringLayout.SOUTH, startDateLabel, LABEL_HEIGHT, SpringLayout.NORTH, startDateLabel);
        layout.putConstraint(SpringLayout.SOUTH, endDateLabel, LABEL_HEIGHT, SpringLayout.NORTH, endDateLabel);
        layout.putConstraint(SpringLayout.SOUTH, nightNumberLabel, LABEL_HEIGHT, SpringLayout.NORTH, nightNumberLabel);
        layout.putConstraint(SpringLayout.SOUTH, bedNumberLabel, LABEL_HEIGHT, SpringLayout.NORTH, bedNumberLabel);
    }

    public Hotel getHotel() {
        if (hotelJComboBox.getSelectedIndex() > -1)
            return (Hotel) hotelJComboBox.getSelectedItem();
        return null;
    }

    public void setHotel(Hotel hotel) {
        if (hotelJComboBox.getItemCount() > 0) {
            hotelJComboBox.setSelectedItem(hotel);
        }
    }

    public LocalDateTime getStartDate() {
        return startDatePicker.getLocalDateTime();
    }

    public void setStartDate(LocalDateTime date) {
        setStartDate(date, true);
    }

    public void setStartDate(LocalDateTime date, boolean update) {
        startDatePicker.setDate(date, false);

        if (update)
            updateTotalNights();
    }

    public LocalDateTime getEndDate() {
        return endDatePicker.getLocalDateTime();
    }

    public void setEndDate(LocalDateTime date) {
        setEndDate(date, true);
    }

    public void setEndDate(LocalDateTime date, boolean update) {
        endDatePicker.setDate(date, false);

        if (update)
            updateTotalNights();
    }

    public long getTotalNights() {
        LocalDateTime endDate = getEndDate();
        LocalDateTime startDate = getStartDate();

        return ChronoUnit.DAYS.between(startDate, endDate);
    }

    public int getBedNumber() {
        return Integer.parseInt(bedNumberSpinner.getValue().toString());
    }

    public void setBedNumber(int number) {
        bedNumberSpinner.setValue(number);
    }

    public void updateTotalNights() {
        long totalNights = getTotalNights();
        totalNightsLabel.setText(String.format("%d", totalNights));
    }
}
