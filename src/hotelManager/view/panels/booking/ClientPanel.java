package hotelManager.view.panels.booking;

import hotelManager.controllers.AppController;
import hotelManager.controllers.NewBookingController;
import hotelManager.model.Customer;
import hotelManager.model.DbManager;

import javax.swing.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Client panel class
 */
public class ClientPanel extends JPanel {

    private static final int PADDING = 15;
    private static final int LINE_SEPARATION = 35;
    private static final int HORIZONTAL_PADDING = 100;
    private static final int COMPONENT_WIDTH = 200;
    private NewBookingController controller;
    private DefaultListModel<Customer> listModel;
    private JList<Customer> customerList;
    private JButton selectCustomer;
    private JTextField customerSearchField;
    private String filter;
    private List<Customer> allCustomers;
    private SpringLayout layout;

    public ClientPanel(NewBookingController controller) {
        super();
        this.controller = controller;
        initContent();
    }

    private List<Customer> getCustomerList() {
        try {
            allCustomers = DbManager.getInstance().getListCustomer();
            return allCustomers;
        } catch (Exception e) {
            e.printStackTrace();
            AppController.getInstance().showError(e);
        }

        return new ArrayList<>();
    }

    private void initContent() {
        layout = new SpringLayout();
        setLayout(layout);
        customerSearchField = new JTextField();
        customerSearchField.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {

            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {

            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {
                setFilter(customerSearchField.getText());
            }
        });

        listModel = new DefaultListModel<>();
        getCustomerList().stream().forEach(listModel::addElement);
        customerList = new JList<>(listModel);

        selectCustomer = new JButton("Sélectionner client");
        selectCustomer.addActionListener(actionEvent -> {
            if (customerList.getSelectedIndex() > -1)
                controller.selectClient(customerList.getSelectedValue());
            else
                AppController.getInstance().showError("Erreur", "Veuillez sélectionner un client.");
        });

        JScrollPane scrollPane = new JScrollPane(customerList);
        add(customerSearchField);
        add(scrollPane);
        add(selectCustomer);

        layout.putConstraint(SpringLayout.NORTH, customerSearchField, PADDING, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.NORTH, scrollPane, LINE_SEPARATION, SpringLayout.NORTH, customerSearchField);
        layout.putConstraint(SpringLayout.NORTH, selectCustomer, LINE_SEPARATION * 10, SpringLayout.NORTH, scrollPane);

        layout.putConstraint(SpringLayout.WEST, customerSearchField, HORIZONTAL_PADDING, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, scrollPane, HORIZONTAL_PADDING, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, selectCustomer, HORIZONTAL_PADDING, SpringLayout.WEST, this);

        layout.putConstraint(SpringLayout.EAST, customerSearchField, -HORIZONTAL_PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, scrollPane, -HORIZONTAL_PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, selectCustomer, -HORIZONTAL_PADDING, SpringLayout.EAST, this);

        layout.putConstraint(SpringLayout.SOUTH, scrollPane, -PADDING, SpringLayout.NORTH, selectCustomer);
    }

    private void setFilter(String text) {
        text = text.trim();
        if (!Objects.equals(text, filter)) {
            filter = text;
            filterItems();
        }
    }

    private boolean customerMatchFilter(Customer c, String filter) {
        return c.getName().toLowerCase().contains(filter.toLowerCase()) ||
                c.getCustomerNumber().toLowerCase().contains(filter.toLowerCase());
    }

    private void filterItems() {
        // Remove all child
        listModel.removeAllElements();

        // Add only matching items
        if (filter != null && !Objects.equals(filter, "")) {
            allCustomers.stream().filter(i -> customerMatchFilter(i, filter)).forEach(listModel::addElement);
        } else {
            allCustomers.stream().forEach(listModel::addElement);
        }

        customerList.setModel(listModel);
        customerList.updateUI();
        updateUI();
    }

}
