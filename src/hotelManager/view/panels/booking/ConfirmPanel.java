package hotelManager.view.panels.booking;

import hotelManager.controllers.NewBookingController;

import javax.swing.*;

/**
 * Confirm panel
 */
public class ConfirmPanel extends JPanel {

    private NewBookingController controller;

    public ConfirmPanel(NewBookingController controller) {
        this.controller = controller;
        initButton();
    }

    private void initButton() {
        JButton addButton = new JButton("Valider la réservation");
        addButton.addActionListener(actionEvent -> controller.book());
        add(addButton);
    }

}
