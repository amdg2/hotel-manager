package hotelManager.view.panels.booking;

import hotelManager.controllers.NewBookingController;
import hotelManager.model.Room;
import hotelManager.view.component.RoomList;
import hotelManager.view.component.RoomListItem;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * Booking room panel
 */
public class RoomPanel extends JPanel {

    private RoomList roomList;

    public RoomPanel(NewBookingController controller) {
        super(new BorderLayout());
        roomList = new RoomList(controller);
        add(roomList, BorderLayout.CENTER);
    }

    /**
     * Add a list of rooms to panel
     *
     * @param rooms List of rooms
     */
    public void addRoomList(List<Room> rooms) {
        rooms.forEach(this::addRoom);
    }

    /**
     * Add a single room to panel
     *
     * @param room Room to add
     */
    public void addRoom(Room room) {
        roomList.add(new RoomListItem(room));
    }

    public void clearRoomList() {
        roomList.clear();
    }
}
