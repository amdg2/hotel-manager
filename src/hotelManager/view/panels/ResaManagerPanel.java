package hotelManager.view.panels;

import hotelManager.Icons;
import hotelManager.controllers.ResaManagerController;

import javax.swing.*;
import java.awt.*;

/**
 * Reservation manager panel
 */
public class ResaManagerPanel extends JPanel {

    public static final String NEW_BOOKING = "New Booking";
    public static final String RESA_MANAGER = "Resa Manager";

    private CardLayout layout;
    private JPanel mainContent;
    private ResaManagerController controller;

    public ResaManagerPanel(ResaManagerController controller) {
        super(new BorderLayout());
        this.controller = controller;
        initPanelTitle();
        initPanelContent();
    }

    private void initPanelTitle() {
        JLabel title = new JLabel("Gestion des réservations");
        title.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 40));
        title.setHorizontalAlignment(SwingConstants.CENTER);
        add(title, BorderLayout.NORTH);
    }

    private void initPanelContent() {
        layout = new CardLayout();
        mainContent = new JPanel(layout);

        JPanel manager = new JPanel(new BorderLayout());
        JButton bookARoomButton = new JButton("Ajouter une réservation", new ImageIcon(Icons.getAddBookingIcon()));
        bookARoomButton.addActionListener(actionEvent -> showPanel(NEW_BOOKING));
        manager.add(bookARoomButton, BorderLayout.SOUTH);

        mainContent.add(manager, RESA_MANAGER);
        mainContent.add(this.controller.getNewBookingController().getPanel(), NEW_BOOKING);

        add(mainContent, BorderLayout.CENTER);
    }

    public void showPanel(String panel) {
        layout.show(mainContent, panel);
    }
}
