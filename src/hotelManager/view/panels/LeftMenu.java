package hotelManager.view.panels;

import hotelManager.AppConfig;
import hotelManager.Icons;
import hotelManager.controllers.AppController;
import hotelManager.controllers.MouseClickController;
import hotelManager.events.PanelSwitcherClickReceiver;

import javax.swing.*;
import java.awt.*;

/**
 * Left menu
 */
public class LeftMenu extends JPanel {

    private JButton backToHotelListButton;
    private JButton backToHotelDashboardButton;

    private JButton employeeManagerButton;
    private JButton roomManagerButton;
    private JButton resaManagerButton;
    private JButton customerButton;

    private boolean backToHotelDashboardVisible = false;

    private AppController app;

    public LeftMenu(AppController app) {
        this.app = app;
        setLayout(new GridLayout(0, 1));
        setBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, Color.black));
        initButtons();
    }

    private void initButtons() {
        backToHotelDashboardButton = new JButton("Retour à l'accueil hotel", new ImageIcon(Icons.getBackIcon()));
        backToHotelListButton = new JButton("Retour à la liste des hotel", new ImageIcon(Icons.getBackToHotelListIcon()));

        employeeManagerButton = new JButton("Gestion des employés", new ImageIcon(Icons.getEmployeeManagerIcon()));
        roomManagerButton = new JButton("Gestion des chambres", new ImageIcon(Icons.getRoomManagerIcon()));
        resaManagerButton = new JButton("Gestion des réservations", new ImageIcon(Icons.getResaManagerIcon()));
        customerButton = new JButton("Gestion des clients", new ImageIcon(Icons.getCustomerManagerIcon()));

        setButtonLayout(backToHotelDashboardButton, AppController.PANEL_HOTEL_WELCOME);
        setButtonLayout(backToHotelListButton, AppController.PANEL_APP_WELCOME);
        setButtonLayout(employeeManagerButton, AppController.PANEL_EMPLOYEE_MANAGER);
        setButtonLayout(roomManagerButton, AppController.PANEL_ROOM_MANAGER);
        setButtonLayout(resaManagerButton, AppController.PANEL_RESA_MANAGER);
        setButtonLayout(customerButton, AppController.PANEL_CUSTOMER_MANAGER);

        addButtons(backToHotelListButton);
    }

    private void addButtons(JButton backButton) {
        add(backButton);
        add(employeeManagerButton);
        add(roomManagerButton);
        add(resaManagerButton);
        add(customerButton);
    }

    private void setButtonLayout(JButton button, int panelToOpenOnClick) {
        button.setVerticalTextPosition(SwingConstants.BOTTOM);
        button.setHorizontalTextPosition(SwingConstants.CENTER);
        button.setPreferredSize(new Dimension(200, 150));
        button.setBorderPainted(false);
        button.setBackground(AppConfig.BTN_BACKGROUND_COLOR);

        button.addMouseListener(new MouseClickController(new PanelSwitcherClickReceiver(app, panelToOpenOnClick)));
    }

    public void showBackToHotelDashboardButton() {
        if (!backToHotelDashboardVisible) {
            removeAll();
            addButtons(backToHotelDashboardButton);
            updateUI();
            backToHotelDashboardVisible = true;
        }
    }

    public void showBackToHotelListButton() {
        if (backToHotelDashboardVisible) {
            removeAll();
            addButtons(backToHotelListButton);
            updateUI();
            backToHotelDashboardVisible = false;
        }
    }
}
