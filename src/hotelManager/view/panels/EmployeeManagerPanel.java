package hotelManager.view.panels;

import hotelManager.Icons;
import hotelManager.controllers.EmployeeEditorController;
import hotelManager.controllers.EmployeeManagerController;
import hotelManager.model.Employee;
import hotelManager.model.Hotel;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import java.awt.*;

/**
 * Employee manager panel
 */
public class EmployeeManagerPanel extends JPanel {

    private static final String EMPLOYEE_EDITOR = "Employee Editor";
    private static final String EMPLOYEE_MANAGER_HOME = "Employee Manager Home";

    private JList<Employee> employeeList;

    private Hotel hotel = null;

    private EmployeeManagerController controller;
    private EmployeeEditorController employeeEditor;
    private JPanel contentSwitcher;
    private JPanel managerHome;
    private CardLayout contentSwitcherLayout;

    public EmployeeManagerPanel(EmployeeManagerController controller) {
        super();
        this.controller = controller;
        setLayout(new BorderLayout());
        initPanelHeader();
        initPanelContent();
    }

    private void initPanelHeader() {
        JPanel headerPanel = new JPanel(new BorderLayout());
        JLabel panelTitle = new JLabel("Gestion des employés");

        panelTitle.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 40));
        panelTitle.setHorizontalAlignment(SwingConstants.CENTER);

        headerPanel.add(panelTitle, BorderLayout.CENTER);
        add(headerPanel, BorderLayout.NORTH);
    }

    private void initPanelContent() {
        employeeList = new JList<>();
        employeeList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        employeeList.addListSelectionListener(this::employeeSelected);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setPreferredSize(new Dimension(200, 200));
        scrollPane.setViewportView(employeeList);
        add(scrollPane, BorderLayout.WEST);

        contentSwitcher = new JPanel(new CardLayout());
        contentSwitcherLayout = (CardLayout) contentSwitcher.getLayout();

        employeeEditor = new EmployeeEditorController(controller);
        initManagerHome();

        contentSwitcher.add(employeeEditor.getPanel(), EMPLOYEE_EDITOR);
        contentSwitcher.add(managerHome, EMPLOYEE_MANAGER_HOME);

        add(contentSwitcher, BorderLayout.CENTER);
        contentSwitcherLayout.show(contentSwitcher, EMPLOYEE_MANAGER_HOME);
    }

    private void employeeSelected(ListSelectionEvent event) {
        if (!employeeList.isSelectionEmpty()) {
            selectEmployee(employeeList.getSelectedValue());
        }
    }

    public void selectEmployee(Employee e) {
        if (e != null) {
            employeeEditor.setEmployee(e);
            showEmployeeEditor();
        }
    }

    private void initManagerHome() {
        managerHome = new JPanel(new BorderLayout());

        JLabel text = new JLabel("<html><div style='text-align: center;'>Veuillez sélectionner un employé pour voir et éditer ses informations.<br>Ou utilisez le bouton ci-dessous pour ajouter un employé.</div></html>");
        text.setVerticalTextPosition(JLabel.CENTER);
        text.setHorizontalAlignment(JLabel.CENTER);
        managerHome.add(text, BorderLayout.CENTER);

        JButton button = new JButton("Ajouter un employé", new ImageIcon(Icons.getPersonAddIcon()));
        button.addActionListener(actionEvent -> showNewEmployeeEditor());
        managerHome.add(button, BorderLayout.SOUTH);
    }

    private void updateData() {
        showPanel(EMPLOYEE_MANAGER_HOME);
    }

    public void setEmployeeList(Employee[] employees) {
        employeeList.setListData(employees);
    }

    public EmployeeEditorController getEditorController() {
        return employeeEditor;
    }

    public void showManagerHome() {
        showPanel(EMPLOYEE_MANAGER_HOME);
    }

    public void showEmployeeEditor() {
        employeeEditor.openEditEmployeeEditor();
        showPanel(EMPLOYEE_EDITOR);
    }

    public void showNewEmployeeEditor() {
        employeeEditor.openNewEmployeeEditor();
        showPanel(EMPLOYEE_EDITOR);
    }

    private void showPanel(String panelName) {
        contentSwitcherLayout.show(contentSwitcher, panelName);
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        if (hotel == this.hotel)
            return;

        this.hotel = hotel;
        updateData();
    }

    public JList<Employee> getListEmployee() {
        return employeeList;
    }
}
