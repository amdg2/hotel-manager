package hotelManager.view.panels;

import hotelManager.Icons;
import hotelManager.controllers.AppController;
import hotelManager.controllers.CustomerEditorController;
import hotelManager.model.*;
import hotelManager.utils.CustomDatePicker;
import hotelManager.utils.CustomSpinner;
import hotelManager.utils.CustomTextField;

import javax.swing.*;
import java.util.Date;

/**
 * Customer editor panel
 *
 * @author Kevin
 */
public class CustomerEditorPanel extends JPanel {

    private static final int LABEL_HEIGHT = 30;
    private static final int LINE_SEPARATION = 35;
    private static final int INPUT_WIDTH = 300;
    private static final int PADDING = 15;
    private Hotel hotel;
    private Customer customer;
    private SpringLayout layout;
    private JLabel labelNom;
    private JLabel labelDateNaissance;
    private JLabel labelNumClient;
    private JLabel labelAdresse;
    private CustomTextField inputNom;
    private CustomDatePicker inputDateNaissance;
    private CustomTextField inputNumClient;
    private CustomTextField inputAdresse;
    private CustomSpinner inputCodePostal;
    private JButton buttonValidateCodePostal;
    private JComboBox<City> inputVille;
    private JButton buttonSave;
    private JButton buttonDelete;
    private CustomerEditorController controller;
    private JButton buttonAdd;
    private JButton buttonNew;

    public CustomerEditorPanel(CustomerEditorController controller) {
        super();
        this.controller = controller;
        layout = new SpringLayout();
        setLayout(layout);

        initLabels();
        initInputs();
        initButtons();
    }

    private void initButtons() {
        buttonNew = new JButton("Ajouter un client", new ImageIcon(Icons.getPersonAddSmallIcon()));
        buttonAdd = new JButton("Ajouter", new ImageIcon(Icons.getPersonAddSmallIcon()));
        buttonSave = new JButton("Enregistrer", new ImageIcon(Icons.getSaveIcon()));
        buttonDelete = new JButton("Supprimer", new ImageIcon(Icons.getDeleteIcon()));

        buttonSave.addActionListener(actionEvent -> controller.saveCustomer());
        buttonAdd.addActionListener(actionEvent -> controller.addCustomer());
        buttonNew.addActionListener(actionEvent -> controller.openNewCustomerEditor());
        buttonDelete.addActionListener(actionEvent -> askToDeleteCustomer());


        add(buttonNew);
        add(buttonAdd);
        add(buttonSave);
        add(buttonDelete);

        layout.putConstraint(SpringLayout.SOUTH, buttonNew, -PADDING, SpringLayout.SOUTH, this);
        layout.putConstraint(SpringLayout.SOUTH, buttonAdd, -PADDING, SpringLayout.SOUTH, this);
        layout.putConstraint(SpringLayout.SOUTH, buttonSave, -PADDING, SpringLayout.SOUTH, this);
        layout.putConstraint(SpringLayout.SOUTH, buttonDelete, -PADDING, SpringLayout.SOUTH, this);

        layout.putConstraint(SpringLayout.WEST, buttonNew, PADDING, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.EAST, buttonAdd, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, buttonSave, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, buttonDelete, -PADDING, SpringLayout.WEST, buttonSave);
    }

    private void initLabels() {
        labelNom = new JLabel("Nom");
        labelDateNaissance = new JLabel("Date de Naissance");
        labelNumClient = new JLabel("Numéro Client");
        labelAdresse = new JLabel("Adresse");

        labelNom.setVerticalTextPosition(JLabel.CENTER);
        labelDateNaissance.setVerticalTextPosition(JLabel.CENTER);
        labelNumClient.setVerticalTextPosition(JLabel.CENTER);
        labelAdresse.setVerticalTextPosition(JLabel.CENTER);

        add(labelNom);
        add(labelDateNaissance);
        add(labelNumClient);
        add(labelAdresse);

        layout.putConstraint(SpringLayout.WEST, labelNom, PADDING, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, labelDateNaissance, PADDING, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, labelNumClient, PADDING, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, labelAdresse, PADDING, SpringLayout.WEST, this);

        layout.putConstraint(SpringLayout.NORTH, labelNom, LINE_SEPARATION, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.NORTH, labelDateNaissance, LINE_SEPARATION, SpringLayout.NORTH, labelNom);
        layout.putConstraint(SpringLayout.NORTH, labelNumClient, LINE_SEPARATION, SpringLayout.NORTH, labelDateNaissance);
        layout.putConstraint(SpringLayout.NORTH, labelAdresse, LINE_SEPARATION, SpringLayout.NORTH, labelNumClient);

        layout.putConstraint(SpringLayout.SOUTH, labelNom, LABEL_HEIGHT, SpringLayout.NORTH, labelNom);
        layout.putConstraint(SpringLayout.SOUTH, labelDateNaissance, LABEL_HEIGHT, SpringLayout.NORTH, labelDateNaissance);
        layout.putConstraint(SpringLayout.SOUTH, labelNumClient, LABEL_HEIGHT, SpringLayout.NORTH, labelNumClient);
        layout.putConstraint(SpringLayout.SOUTH, labelAdresse, LABEL_HEIGHT, SpringLayout.NORTH, labelAdresse);
    }

    private void initInputs() {
        inputNom = new CustomTextField();
        inputDateNaissance = new CustomDatePicker();
        inputNumClient = new CustomTextField();
        inputAdresse = new CustomTextField();
        inputCodePostal = new CustomSpinner(new SpinnerNumberModel());
        buttonValidateCodePostal = new JButton("Valider Code Postal", new ImageIcon(Icons.getValidateIcon()));
        inputVille = new JComboBox<>();

        inputVille.setEnabled(false);

        add(inputNom);
        add(inputDateNaissance);
        add(inputNumClient);
        add(inputAdresse);
        add(inputCodePostal);
        add(buttonValidateCodePostal);
        add(inputVille);

        layout.putConstraint(SpringLayout.EAST, inputNom, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, inputDateNaissance, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, inputNumClient, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, inputAdresse, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, buttonValidateCodePostal, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, inputCodePostal, -10, SpringLayout.EAST, buttonValidateCodePostal);
        layout.putConstraint(SpringLayout.EAST, inputVille, -PADDING, SpringLayout.EAST, this);

        layout.putConstraint(SpringLayout.NORTH, inputNom, LINE_SEPARATION, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.NORTH, inputDateNaissance, LINE_SEPARATION, SpringLayout.NORTH, inputNom);
        layout.putConstraint(SpringLayout.NORTH, inputNumClient, LINE_SEPARATION + 5, SpringLayout.NORTH, inputDateNaissance);
        layout.putConstraint(SpringLayout.NORTH, inputAdresse, LINE_SEPARATION, SpringLayout.NORTH, inputNumClient);
        layout.putConstraint(SpringLayout.NORTH, inputCodePostal, LINE_SEPARATION, SpringLayout.NORTH, inputAdresse);
        layout.putConstraint(SpringLayout.NORTH, buttonValidateCodePostal, LINE_SEPARATION, SpringLayout.NORTH, inputAdresse);
        layout.putConstraint(SpringLayout.NORTH, inputVille, LINE_SEPARATION, SpringLayout.NORTH, buttonValidateCodePostal);

        layout.putConstraint(SpringLayout.WEST, inputNom, -(PADDING + INPUT_WIDTH), SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.WEST, inputDateNaissance, -(PADDING + INPUT_WIDTH), SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.WEST, inputAdresse, -(PADDING + INPUT_WIDTH), SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.WEST, inputVille, -(PADDING + INPUT_WIDTH), SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.WEST, inputNumClient, -(PADDING + INPUT_WIDTH), SpringLayout.EAST, this);

        layout.putConstraint(SpringLayout.WEST, inputCodePostal, -(PADDING + INPUT_WIDTH), SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, inputCodePostal, -10, SpringLayout.WEST, buttonValidateCodePostal);

        inputNumClient.setHorizontalAlignment(JCheckBox.RIGHT);

        buttonValidateCodePostal.addActionListener(actionEvent -> loadVille());
    }

    private void loadVille() {
        int codepostal = Integer.parseInt(inputCodePostal.getValue().toString());
        inputVille.removeAllItems();

        try {
            DbManager.getInstance().getCityByZipCode(codepostal).forEach(ville -> inputVille.addItem(ville));
            inputVille.setEnabled(inputVille.getItemCount() > 0);
        } catch (Exception e) {
            AppController.getInstance().showError(e);
        }
    }

    private void loadData() {
        inputNom.setText(customer.getName());
        inputDateNaissance.setDate(customer.getDateOfBirth());
        inputNumClient.setText(customer.getCustomerNumber());
        inputAdresse.setText(customer.getAddress());

        if (customer.getCity() != null && customer.getCity().getZipCode() != null) {
            int codePostal = Integer.parseInt(customer.getCity().getZipCode());
            inputCodePostal.setValue(codePostal);
        }

        loadVille();
        if (inputVille.getItemCount() > 0)
            inputVille.setSelectedItem(customer.getCity());
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setCustomer(Customer e) {
        if (customer == e)
            return;

        customer = e;
        loadData();
    }

    public String getNom() {
        return inputNom.getText();
    }

    public void setNom(String nom) {
        inputNom.setText(nom);
    }

    public Date getDateNaissance() {
        return inputDateNaissance.getDate();
    }

    public void setDateNaissance(Date d) {
        inputDateNaissance.setDate(d);
    }

    public String getNumClient() {
        return inputNumClient.getText();
    }

    public void setNumClient(String num) {
        inputNumClient.setText(num);
    }

    public String getAdresse() {
        return inputAdresse.getText();
    }

    public void setAdresse(String addr) {
        inputAdresse.setText(addr);
    }

    public String getCodePostal() {
        return inputCodePostal.getValue().toString();
    }

    public City getVille() {
        return (City) inputVille.getSelectedItem();
    }

    public void setVille(City c) {
        inputCodePostal.setValue(Integer.parseInt(c.getZipCode()));
        loadVille();
        inputVille.setSelectedItem(c);
    }

    public void switchToAddCustomer() {
        buttonAdd.setVisible(true);
        buttonDelete.setVisible(false);
        buttonSave.setVisible(false);
        buttonNew.setVisible(false);
    }

    public void switchToEditCustomer() {
        buttonAdd.setVisible(false);
        buttonDelete.setVisible(true);
        buttonSave.setVisible(true);
        buttonNew.setVisible(true);
    }

    public void clear() {
        inputNom.setText("");
        inputDateNaissance.setDate(new Date());
        inputNumClient.setText("");
        inputAdresse.setText("");
        inputCodePostal.setValue(0);
        loadVille();
    }

    private void askToDeleteCustomer() {

        try {

            if (JOptionPane.showConfirmDialog(this,
                    "Voulez-vous vraiment supprimer ce client ?",
                    "Confirmation",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE) == JOptionPane.OK_OPTION) {

                if (DbManager.getInstance().hadReservation(customer)) {
                    if (JOptionPane.showConfirmDialog(this,
                            "<html><div style='text-align : center'>Ce client a des réservations en cours. Voulez vous vraiment le supprimer ? <br/>" +
                                    "Toutes statistiques et données de réservations seront perdues.</div></html>",
                            "Confirmation",
                            JOptionPane.YES_NO_OPTION,
                            JOptionPane.QUESTION_MESSAGE) == JOptionPane.OK_OPTION)
                        controller.deleteCustomer();
                }else{
                    controller.deleteCustomer();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
