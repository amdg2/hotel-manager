package hotelManager.view.panels;

import hotelManager.AppConfig;
import hotelManager.Icons;
import hotelManager.PropertyLoader;
import hotelManager.controllers.AppController;
import hotelManager.controllers.MouseClickController;
import hotelManager.controllers.SettingsController;
import hotelManager.events.PanelSwitcherClickReceiver;
import hotelManager.utils.CustomTextField;

import javax.swing.*;
import java.awt.*;
import java.util.Properties;

/**
 * Settings panel
 */
public class SettingsPanel extends JPanel {



    private static final int LABEL_HEIGHT = 30;
    private static final int LINE_SEPARATION = 35;
    private static final int INPUT_WIDTH = 300;
    private static final int PADDING = 15;

    private SpringLayout layout;


    private JLabel labelNomChaineHotel;
    private JLabel labelJdbcUsername;
    private JLabel labelJdbcPort;
    private JLabel labelJdbcTypeDb;
    private JLabel labelJdbcHostname;
    private JLabel labelJdbcPassword;
    private JLabel labelJdbcSid;
    private CustomTextField inputNomChaineHotel;
    private CustomTextField inputJdbcUsername;
    private CustomTextField inputJdbcPort;
    private CustomTextField inputJdbcTypeDb;
    private CustomTextField inputJdbcHostname;
    private JPasswordField inputJdbcPassword;
    private CustomTextField inputJdbcSid;
    private JButton backButton;
    private JButton saveButton;




    private SettingsController controller;

    public SettingsPanel(SettingsController controller) {
        super();
        this.controller = controller;
        layout = new SpringLayout();
        setLayout(layout);

        initPanelHeader();
        initButton();
        initLabels();
        initInputs();
    }

    private void initLabels() {
        labelNomChaineHotel = new JLabel("Nom de la chaine d'hotel");
        labelJdbcUsername = new JLabel("Nom utilisateur");
        labelJdbcPort = new JLabel("Port");
        labelJdbcTypeDb = new JLabel("Type de base de données");
        labelJdbcHostname = new JLabel("Adresse d'hébergement");
        labelJdbcPassword = new JLabel("Mot de passe");
        labelJdbcSid = new JLabel("SID");

        labelNomChaineHotel.setVerticalTextPosition(JLabel.CENTER);
        labelJdbcUsername.setVerticalTextPosition(JLabel.CENTER);
        labelJdbcPort.setVerticalTextPosition(JLabel.CENTER);
        labelJdbcTypeDb.setVerticalTextPosition(JLabel.CENTER);
        labelJdbcHostname.setVerticalTextPosition(JLabel.CENTER);
        labelJdbcPassword.setVerticalTextPosition(JLabel.CENTER);
        labelJdbcSid.setVerticalTextPosition(JLabel.CENTER);

        add(labelNomChaineHotel);
        add(labelJdbcUsername);
        add(labelJdbcPort);
        add(labelJdbcTypeDb);
        add(labelJdbcHostname);
        add(labelJdbcPassword);
        add(labelJdbcSid);

        layout.putConstraint(SpringLayout.WEST, labelNomChaineHotel, PADDING, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, labelJdbcUsername, PADDING, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, labelJdbcPort, PADDING, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, labelJdbcTypeDb, PADDING, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, labelJdbcHostname, PADDING, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, labelJdbcPassword, PADDING, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, labelJdbcSid, PADDING, SpringLayout.WEST, this);

        layout.putConstraint(SpringLayout.NORTH, labelNomChaineHotel, LINE_SEPARATION * 2, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.NORTH, labelJdbcUsername, LINE_SEPARATION, SpringLayout.NORTH, labelNomChaineHotel);
        layout.putConstraint(SpringLayout.NORTH, labelJdbcPort, LINE_SEPARATION, SpringLayout.NORTH, labelJdbcUsername);
        layout.putConstraint(SpringLayout.NORTH, labelJdbcTypeDb, LINE_SEPARATION, SpringLayout.NORTH, labelJdbcPort);
        layout.putConstraint(SpringLayout.NORTH, labelJdbcHostname, LINE_SEPARATION, SpringLayout.NORTH, labelJdbcTypeDb);
        layout.putConstraint(SpringLayout.NORTH, labelJdbcPassword, LINE_SEPARATION, SpringLayout.NORTH, labelJdbcHostname);
        layout.putConstraint(SpringLayout.NORTH, labelJdbcSid, LINE_SEPARATION, SpringLayout.NORTH, labelJdbcPassword);

        layout.putConstraint(SpringLayout.SOUTH, labelNomChaineHotel, LABEL_HEIGHT, SpringLayout.NORTH, labelNomChaineHotel);
        layout.putConstraint(SpringLayout.SOUTH, labelJdbcUsername, LABEL_HEIGHT, SpringLayout.NORTH, labelJdbcUsername);
        layout.putConstraint(SpringLayout.SOUTH, labelJdbcPort, LABEL_HEIGHT, SpringLayout.NORTH, labelJdbcPort);
        layout.putConstraint(SpringLayout.SOUTH, labelJdbcTypeDb, LABEL_HEIGHT, SpringLayout.NORTH, labelJdbcTypeDb);
        layout.putConstraint(SpringLayout.SOUTH, labelJdbcHostname, LABEL_HEIGHT, SpringLayout.NORTH, labelJdbcHostname);
        layout.putConstraint(SpringLayout.SOUTH, labelJdbcPassword, LABEL_HEIGHT, SpringLayout.NORTH, labelJdbcPassword);
        layout.putConstraint(SpringLayout.SOUTH, labelJdbcSid, LABEL_HEIGHT, SpringLayout.NORTH, labelJdbcSid);

    }

    private void initInputs() {
        inputNomChaineHotel = new CustomTextField();
        inputJdbcUsername = new CustomTextField();
        inputJdbcPort = new CustomTextField();
        inputJdbcTypeDb = new CustomTextField();
        inputJdbcHostname = new CustomTextField();
        inputJdbcPassword = new JPasswordField();
        inputJdbcSid = new CustomTextField();

        add(inputNomChaineHotel);
        add(inputJdbcUsername);
        add(inputJdbcPort);
        add(inputJdbcTypeDb);
        add(inputJdbcHostname);
        add(inputJdbcPassword);
        add(inputJdbcSid);

        layout.putConstraint(SpringLayout.EAST, inputNomChaineHotel, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, inputJdbcUsername, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, inputJdbcPort, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, inputJdbcTypeDb, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, inputJdbcHostname, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, inputJdbcPassword, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, inputJdbcSid, -PADDING, SpringLayout.EAST, this);

        layout.putConstraint(SpringLayout.NORTH, inputNomChaineHotel, LINE_SEPARATION*2, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.NORTH, inputJdbcUsername, LINE_SEPARATION, SpringLayout.NORTH, inputNomChaineHotel);
        layout.putConstraint(SpringLayout.NORTH, inputJdbcPort, LINE_SEPARATION, SpringLayout.NORTH, inputJdbcUsername);
        layout.putConstraint(SpringLayout.NORTH, inputJdbcTypeDb, LINE_SEPARATION, SpringLayout.NORTH, inputJdbcPort);
        layout.putConstraint(SpringLayout.NORTH, inputJdbcHostname, LINE_SEPARATION, SpringLayout.NORTH, inputJdbcTypeDb);
        layout.putConstraint(SpringLayout.NORTH, inputJdbcPassword, LINE_SEPARATION, SpringLayout.NORTH, inputJdbcHostname);
        layout.putConstraint(SpringLayout.NORTH, inputJdbcSid, LINE_SEPARATION, SpringLayout.NORTH, inputJdbcPassword);

        layout.putConstraint(SpringLayout.WEST, inputNomChaineHotel, -(PADDING + INPUT_WIDTH), SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.WEST, inputJdbcUsername, -(PADDING + INPUT_WIDTH), SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.WEST, inputJdbcPort, -(PADDING + INPUT_WIDTH), SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.WEST, inputJdbcTypeDb, -(PADDING + INPUT_WIDTH), SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.WEST, inputJdbcHostname, -(PADDING + INPUT_WIDTH), SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.WEST, inputJdbcPassword, -(PADDING + INPUT_WIDTH), SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.WEST, inputJdbcSid, -(PADDING + INPUT_WIDTH), SpringLayout.EAST, this);




    }

    public void loadData(){
        Properties props = PropertyLoader.getProperties();
        inputNomChaineHotel.setText(AppConfig.getHotelChainName());
        inputJdbcUsername.setText(props.getProperty("jdbc.username"));
        inputJdbcPort.setText(props.getProperty("jdbc.port"));
        inputJdbcTypeDb.setText(props.getProperty("jdbc.dbType"));
        inputJdbcPassword.setText(props.getProperty("jdbc.password"));
        inputJdbcHostname.setText(props.getProperty("jdbc.hostname"));
        inputJdbcSid.setText(props.getProperty("jdbc.sid"));
    }

    private void initPanelHeader() {
        JPanel headerPanel = new JPanel(new BorderLayout());
        JLabel panelTitle = new JLabel("Paramètres");

        panelTitle.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 40));
        panelTitle.setHorizontalAlignment(SwingConstants.CENTER);

        headerPanel.add(panelTitle, BorderLayout.CENTER);
        add(headerPanel, BorderLayout.NORTH);
    }

    private void initButton() {
        backButton = new JButton("Retour", new ImageIcon(Icons.getBackIcon()));
        saveButton = new JButton("Enregistrer", new ImageIcon(Icons.getSaveIcon()));
        //backButton.setBorderPainted(false);

        backButton.addMouseListener(new MouseClickController(new PanelSwitcherClickReceiver(controller.getAppController(), AppController.PANEL_APP_WELCOME)));
        saveButton.addActionListener(actionEvent -> saveProperties());

        add(saveButton);
        add(backButton);

        layout.putConstraint(SpringLayout.SOUTH, backButton, -PADDING, SpringLayout.SOUTH, this);
        layout.putConstraint(SpringLayout.SOUTH, saveButton, -PADDING, SpringLayout.SOUTH, this);

        layout.putConstraint(SpringLayout.WEST, backButton, PADDING, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.EAST, saveButton, -PADDING, SpringLayout.EAST, this);
    }

    private void saveProperties() {
        Properties prop = PropertyLoader.getProperties();

        prop.setProperty("hotel.chain.name",inputNomChaineHotel.getText());
        prop.setProperty("jdbc.port",inputJdbcPort.getText());
        prop.setProperty("jdbc.dbType",inputJdbcTypeDb.getText());
        prop.setProperty("jdbc.password",String.valueOf(inputJdbcPassword.getPassword()));
        prop.setProperty("jdbc.hostname",inputJdbcHostname.getText());
        prop.getProperty("jdbc.sid",inputJdbcSid.getText());
        prop.setProperty("jdbc.username",inputJdbcUsername.getText());

        PropertyLoader.writeProperties(prop);
    }
}
