package hotelManager.view.panels;

import hotelManager.model.Employee;
import hotelManager.model.Hotel;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * Hotel dashboard panel class
 */
public class HotelDashboardPanel extends JPanel {

    private static final int LINE_SEPARATION = 18;
    private static final int PADDING_RIGHT = -15;
    private static final int PADDING_LEFT = 15;
    private static final int TAB = 15;
    private Hotel hotel;
    private JPanel headerPanel;
    private JPanel hotelSummary;
    private JLabel labelGeneralInfo;
    private JLabel labelStars;
    private JLabel hotelStars;
    private JLabel labelAddress;
    private JLabel hotelAddress;
    private JLabel labelManager;
    private JLabel hotelManager;
    private JLabel labelNbrRooms;
    private JLabel labelNbrSingleRooms;
    private JLabel hotelNbrSingleRooms;
    private JLabel labelNbrTwinRooms;
    private JLabel hotelNbrTwinRooms;
    private JLabel hotelName;
    private SpringLayout hotelSummaryLayout;

    public HotelDashboardPanel() {
        super();
        //setLayout(new GridBagLayout());
        setLayout(new BorderLayout());

        initHeader();
        initHotelSummary();
    }

    private void initHotelSummary() {
        hotelSummaryLayout = new SpringLayout();
        hotelSummary = new JPanel(hotelSummaryLayout);

        labelGeneralInfo = new JLabel("Informations générales :");
        labelStars = new JLabel("- Nombre d'étoiles :");
        labelAddress = new JLabel("- Adresse :");
        labelManager = new JLabel("- Manager :");
        labelNbrRooms = new JLabel("- Nombre de chambres avec :");
        labelNbrTwinRooms = new JLabel("\t- Lit double :");
        labelNbrSingleRooms = new JLabel("\t- Lit simple :");

        hotelStars = new JLabel("{stars}");
        hotelAddress = new JLabel("{adress}");
        hotelManager = new JLabel("{manager}");
        hotelNbrTwinRooms = new JLabel("{nbr_twin_rooms}");
        hotelNbrSingleRooms = new JLabel("{nbr_single_rooms}");

        hotelSummary.add(labelGeneralInfo);
        hotelSummary.add(labelStars);
        hotelSummary.add(labelAddress);
        hotelSummary.add(labelManager);
        hotelSummary.add(labelNbrRooms);
        hotelSummary.add(labelNbrTwinRooms);
        hotelSummary.add(labelNbrSingleRooms);

        hotelSummary.add(hotelStars);
        hotelSummary.add(hotelAddress);
        hotelSummary.add(hotelManager);
        hotelSummary.add(hotelNbrTwinRooms);
        hotelSummary.add(hotelNbrSingleRooms);

        hotelSummaryLayout.putConstraint(SpringLayout.NORTH, labelGeneralInfo, LINE_SEPARATION, SpringLayout.NORTH, hotelSummary);
        hotelSummaryLayout.putConstraint(SpringLayout.NORTH, labelStars, LINE_SEPARATION, SpringLayout.NORTH, labelGeneralInfo);
        hotelSummaryLayout.putConstraint(SpringLayout.NORTH, labelAddress, LINE_SEPARATION, SpringLayout.NORTH, labelStars);
        hotelSummaryLayout.putConstraint(SpringLayout.NORTH, labelManager, LINE_SEPARATION, SpringLayout.NORTH, labelAddress);
        hotelSummaryLayout.putConstraint(SpringLayout.NORTH, labelNbrRooms, LINE_SEPARATION, SpringLayout.NORTH, labelManager);
        hotelSummaryLayout.putConstraint(SpringLayout.NORTH, labelNbrTwinRooms, LINE_SEPARATION, SpringLayout.NORTH, labelNbrRooms);
        hotelSummaryLayout.putConstraint(SpringLayout.NORTH, labelNbrSingleRooms, LINE_SEPARATION, SpringLayout.NORTH, labelNbrTwinRooms);

        hotelSummaryLayout.putConstraint(SpringLayout.WEST, labelGeneralInfo, PADDING_LEFT, SpringLayout.WEST, hotelSummary);
        hotelSummaryLayout.putConstraint(SpringLayout.WEST, labelStars, PADDING_LEFT, SpringLayout.WEST, hotelSummary);
        hotelSummaryLayout.putConstraint(SpringLayout.WEST, labelAddress, PADDING_LEFT, SpringLayout.WEST, hotelSummary);
        hotelSummaryLayout.putConstraint(SpringLayout.WEST, labelManager, PADDING_LEFT, SpringLayout.WEST, hotelSummary);
        hotelSummaryLayout.putConstraint(SpringLayout.WEST, labelNbrRooms, PADDING_LEFT, SpringLayout.WEST, hotelSummary);
        hotelSummaryLayout.putConstraint(SpringLayout.WEST, labelNbrTwinRooms, PADDING_LEFT + TAB, SpringLayout.WEST, hotelSummary);
        hotelSummaryLayout.putConstraint(SpringLayout.WEST, labelNbrSingleRooms, PADDING_LEFT + TAB, SpringLayout.WEST, hotelSummary);

        hotelSummaryLayout.putConstraint(SpringLayout.NORTH, hotelStars, LINE_SEPARATION * 2, SpringLayout.NORTH, hotelSummary);
        hotelSummaryLayout.putConstraint(SpringLayout.NORTH, hotelAddress, LINE_SEPARATION, SpringLayout.NORTH, hotelStars);
        hotelSummaryLayout.putConstraint(SpringLayout.NORTH, hotelManager, LINE_SEPARATION, SpringLayout.NORTH, hotelAddress);
        hotelSummaryLayout.putConstraint(SpringLayout.NORTH, hotelNbrTwinRooms, LINE_SEPARATION * 2, SpringLayout.NORTH, hotelManager);
        hotelSummaryLayout.putConstraint(SpringLayout.NORTH, hotelNbrSingleRooms, LINE_SEPARATION, SpringLayout.NORTH, hotelNbrTwinRooms);

        hotelSummaryLayout.putConstraint(SpringLayout.EAST, hotelStars, PADDING_RIGHT, SpringLayout.EAST, hotelSummary);
        hotelSummaryLayout.putConstraint(SpringLayout.EAST, hotelAddress, PADDING_RIGHT, SpringLayout.EAST, hotelSummary);
        hotelSummaryLayout.putConstraint(SpringLayout.EAST, hotelManager, PADDING_RIGHT, SpringLayout.EAST, hotelSummary);
        hotelSummaryLayout.putConstraint(SpringLayout.EAST, hotelNbrTwinRooms, PADDING_RIGHT, SpringLayout.EAST, hotelSummary);
        hotelSummaryLayout.putConstraint(SpringLayout.EAST, hotelNbrSingleRooms, PADDING_RIGHT, SpringLayout.EAST, hotelSummary);

        add(hotelSummary, BorderLayout.CENTER);
    }

    private void initHeader() {
        headerPanel = new JPanel(new BorderLayout());

        //initHotelLogo();
        initHotelName();

        add(headerPanel, BorderLayout.NORTH);
    }

    private void initHotelName() {
        hotelName = new JLabel("{hotelname}");
        hotelName.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 40));
        hotelName.setHorizontalAlignment(SwingConstants.CENTER);
        headerPanel.add(hotelName, BorderLayout.CENTER);
    }

    private String getManagerNameList(List<Employee> managers) {
        if (managers.size() == 0)
            return "Aucun";

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < managers.size() - 1; i++) {
            sb.append(managers.get(i).getName());
            sb.append(", ");
        }
        sb.append(managers.get(managers.size() - 1).getName());

        return sb.toString();
    }

    public void setHotel(Hotel h) {
        hotel = h;

        // Update fields
        hotelName.setText(hotel.getName());
        hotelStars.setText(String.format("%d", hotel.getCategory()));
        hotelAddress.setText(String.format("%s, %s %s", hotel.getAddress(), hotel.getCity().getZipCode(), hotel.getCity().getName()));
        hotelManager.setText(getManagerNameList(hotel.getManager()));
        hotelNbrTwinRooms.setText(String.format("%d", hotel.getTwinRooms().size()));
        hotelNbrSingleRooms.setText(String.format("%d", hotel.getSingleRooms().size()));
    }
}
