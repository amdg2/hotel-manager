package hotelManager.view.panels;

import hotelManager.Icons;
import hotelManager.PropertyLoader;
import hotelManager.controllers.AppController;
import hotelManager.controllers.RoomEditorController;
import hotelManager.model.DbManager;
import hotelManager.model.Hotel;
import hotelManager.model.Room;
import hotelManager.utils.CustomSpinner;

import javax.swing.*;

/**
 * Room Editor Panel
 */
public class RoomEditorPanel extends JPanel{

    private static final int LABEL_HEIGHT = 30;
    private static final int LINE_SEPARATION = 35;
    private static final int INPUT_WIDTH = 300;
    private static final int PADDING = 15;
    private Hotel hotel;
    private Room room;
    private SpringLayout layout;
    private JLabel labelNumber;
    private JLabel labelNbDoubleBed;
    private JLabel labelNbSimpleBed;
    private JLabel labelPrice;
    private JLabel labelStatut;
    private CustomSpinner inputNumber;
    private CustomSpinner inputNbDoubleBed;
    private CustomSpinner inputNbSimpleBed;
    private CustomSpinner inputPrice;
    private JComboBox<String> inputStatut;
    private JButton buttonSave;
    private JButton buttonDelete;
    private RoomEditorController controller;
    private JButton buttonAdd;
    private JButton buttonNew;

    public RoomEditorPanel(RoomEditorController controller){
        super();
        this.controller = controller;
        layout = new SpringLayout();
        setLayout(layout);

        initLabels();
        initInputs();
        initButtons();
        loadFunctions();
    }

    private void initLabels() {
        labelNumber = new JLabel("Numéro de chambre");
        labelNbDoubleBed = new JLabel("Nombre de lits double");
        labelNbSimpleBed = new JLabel("Nombre de lits simple");
        labelPrice = new JLabel("Prix");
        labelStatut = new JLabel("Statut");

        labelNumber.setVerticalTextPosition(JLabel.CENTER);
        labelNbDoubleBed.setVerticalTextPosition(JLabel.CENTER);
        labelNbSimpleBed.setVerticalTextPosition(JLabel.CENTER);
        labelPrice.setVerticalTextPosition(JLabel.CENTER);
        labelStatut.setVerticalTextPosition(JLabel.CENTER);

        add(labelNumber);
        add(labelNbDoubleBed);
        add(labelNbSimpleBed);
        add(labelPrice);
        add(labelStatut);

        layout.putConstraint(SpringLayout.WEST, labelNumber, PADDING, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, labelNbDoubleBed, PADDING, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, labelNbSimpleBed, PADDING, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, labelPrice, PADDING, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, labelStatut, PADDING, SpringLayout.WEST, this);

        layout.putConstraint(SpringLayout.NORTH, labelNumber, LINE_SEPARATION, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.NORTH, labelNbDoubleBed, LINE_SEPARATION, SpringLayout.NORTH, labelNumber);
        layout.putConstraint(SpringLayout.NORTH, labelNbSimpleBed, LINE_SEPARATION, SpringLayout.NORTH, labelNbDoubleBed);
        layout.putConstraint(SpringLayout.NORTH, labelPrice, LINE_SEPARATION, SpringLayout.NORTH, labelNbSimpleBed);
        layout.putConstraint(SpringLayout.NORTH, labelStatut, LINE_SEPARATION, SpringLayout.NORTH, labelPrice);

        layout.putConstraint(SpringLayout.SOUTH, labelNumber, LABEL_HEIGHT, SpringLayout.NORTH, labelNumber);
        layout.putConstraint(SpringLayout.SOUTH, labelNbDoubleBed, LABEL_HEIGHT, SpringLayout.NORTH, labelNbDoubleBed);
        layout.putConstraint(SpringLayout.SOUTH, labelNbSimpleBed, LABEL_HEIGHT, SpringLayout.NORTH, labelNbSimpleBed);
        layout.putConstraint(SpringLayout.SOUTH, labelPrice, LABEL_HEIGHT, SpringLayout.NORTH, labelPrice);
        layout.putConstraint(SpringLayout.SOUTH, labelStatut, LABEL_HEIGHT, SpringLayout.NORTH, labelStatut);

    }

    private void initInputs() {

        inputNumber = new CustomSpinner(new SpinnerNumberModel());
        inputNbDoubleBed = new CustomSpinner(new SpinnerNumberModel());
        inputNbSimpleBed = new CustomSpinner(new SpinnerNumberModel());
        inputPrice = new CustomSpinner(new SpinnerNumberModel());
        inputStatut = new JComboBox<>();

        add(inputNumber);
        add(inputNbDoubleBed);
        add(inputNbSimpleBed);
        add(inputPrice);
        add(inputStatut);

        layout.putConstraint(SpringLayout.EAST, inputNumber, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, inputNbDoubleBed, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, inputNbSimpleBed, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, inputPrice, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, inputStatut, -PADDING, SpringLayout.EAST, this);

        layout.putConstraint(SpringLayout.NORTH, inputNumber, LINE_SEPARATION, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.NORTH, inputNbDoubleBed, LINE_SEPARATION, SpringLayout.NORTH, inputNumber);
        layout.putConstraint(SpringLayout.NORTH, inputNbSimpleBed, LINE_SEPARATION + 5, SpringLayout.NORTH, inputNbDoubleBed);
        layout.putConstraint(SpringLayout.NORTH, inputPrice, LINE_SEPARATION, SpringLayout.NORTH, inputNbSimpleBed);
        layout.putConstraint(SpringLayout.NORTH, inputStatut, LINE_SEPARATION, SpringLayout.NORTH, inputPrice);

        layout.putConstraint(SpringLayout.WEST, inputNumber, -(PADDING + INPUT_WIDTH), SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.WEST, inputNbDoubleBed, -(PADDING + INPUT_WIDTH), SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.WEST, inputPrice, -(PADDING + INPUT_WIDTH), SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.WEST, inputStatut, -(PADDING + INPUT_WIDTH), SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.WEST, inputNbSimpleBed, -(PADDING + INPUT_WIDTH), SpringLayout.EAST, this);
    }

    private void loadFunctions() {
        if (inputStatut.getItemCount() == 0) {
            String[] statut_list = PropertyLoader.getProperties().getProperty("room.statut.list").split(",");
            try {
                for (String statut: statut_list) {
                    inputStatut.addItem(statut);
                }

            } catch (Exception e) {
                AppController.getInstance().showError(e);
            }
        }
    }

    private void initButtons() {
        buttonNew = new JButton("Ajouter une Chambre", new ImageIcon(Icons.getPersonAddSmallIcon()));
        buttonAdd = new JButton("Ajouter", new ImageIcon(Icons.getPersonAddSmallIcon()));
        buttonSave = new JButton("Enregistrer", new ImageIcon(Icons.getSaveIcon()));
        buttonDelete = new JButton("Supprimer", new ImageIcon(Icons.getDeleteIcon()));

        buttonSave.addActionListener(actionEvent -> controller.saveRoom());
        buttonAdd.addActionListener(actionEvent -> controller.addRoom());
        buttonNew.addActionListener(actionEvent -> controller.openNewRoomEditor());
        buttonDelete.addActionListener(actionEvent -> askToDeleteRoom());

        add(buttonNew);
        add(buttonAdd);
        add(buttonSave);
        add(buttonDelete);

        layout.putConstraint(SpringLayout.SOUTH, buttonNew, -PADDING, SpringLayout.SOUTH, this);
        layout.putConstraint(SpringLayout.SOUTH, buttonAdd, -PADDING, SpringLayout.SOUTH, this);
        layout.putConstraint(SpringLayout.SOUTH, buttonSave, -PADDING, SpringLayout.SOUTH, this);
        layout.putConstraint(SpringLayout.SOUTH, buttonDelete, -PADDING, SpringLayout.SOUTH, this);

        layout.putConstraint(SpringLayout.WEST, buttonNew, PADDING, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.EAST, buttonAdd, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, buttonSave, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, buttonDelete, -PADDING, SpringLayout.WEST, buttonSave);

    }

    public int getNumber() {
        return Integer.parseInt(inputNumber.getValue().toString());
    }

    public void setNumber(int number) {
        this.inputNumber.setValue(number);
    }

    public int getNbDoubleBed() {
        return Integer.parseInt(inputNbDoubleBed.getValue().toString());
    }

    public void setNbDoubleBed(int nbDoubleBed) {
        this.inputNbDoubleBed.setValue(nbDoubleBed);
    }

    public int getNbSimpleBed() {
        return Integer.parseInt(inputNbSimpleBed.getValue().toString());
    }

    public void setNbSimpleBed(int nbSimpleBed) {
        this.inputNbSimpleBed.setValue(nbSimpleBed);
    }

    public float getPrice() {
        return Float.parseFloat(inputPrice.getValue().toString());
    }

    public void setPrice(float price) {
        this.inputPrice.setValue(inputPrice);
    }

    public String getStatut() {
        return (String) inputStatut.getSelectedItem();
    }

    public void setStatut(String statut) {
        this.inputStatut.setSelectedItem(statut);
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setRoom(Room r) {
        if (room == r)
            return;

        room = r;
        hotel = room.getHotel();

        loadData();
    }

    private void loadData() {
        inputNumber.setValue(room.getNumber());
        inputNbDoubleBed.setValue(room.getNbDoubleBed());
        inputNbSimpleBed.setValue(room.getNbSimpleBed());
        inputPrice.setValue(room.getPrice());
        inputStatut.setSelectedItem(room.getStatut());

    }

    public void switchToAddRoom() {
        buttonAdd.setVisible(true);
        buttonDelete.setVisible(false);
        buttonSave.setVisible(false);
        buttonNew.setVisible(false);
    }

    public void switchToEditRoom() {
        buttonAdd.setVisible(false);
        buttonDelete.setVisible(true);
        buttonSave.setVisible(true);
        buttonNew.setVisible(true);
    }
    public void clear() {
        inputNumber.setValue(0);
        inputNbDoubleBed.setValue(0);
        inputNbSimpleBed.setValue(0);
        inputPrice.setValue(0);
        inputStatut.setSelectedIndex(0);
    }

    private void askToDeleteRoom() {

        try {

            if (JOptionPane.showConfirmDialog(this,
                    "Voulez-vous vraiment supprimer cette chambre ?",
                    "Confirmation",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE) == JOptionPane.OK_OPTION) {

                if (DbManager.getInstance().isReserved(room)) {
                    if (JOptionPane.showConfirmDialog(this,
                            "<html><div style='text-align : center'>Cette chambre a des réservations en cours. Voulez vous vraiment la supprimer ? <br/>" +
                            "Toutes statistiques et données de réservations seront perdues.</div></html>",
                            "Confirmation",
                            JOptionPane.YES_NO_OPTION,
                            JOptionPane.QUESTION_MESSAGE) == JOptionPane.OK_OPTION)
                        controller.deleteRoom();
                }else{
                    controller.deleteRoom();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
