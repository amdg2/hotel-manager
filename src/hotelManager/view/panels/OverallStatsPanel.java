package hotelManager.view.panels;

import hotelManager.Icons;
import hotelManager.controllers.AppController;
import hotelManager.controllers.MouseClickController;
import hotelManager.controllers.OverallStatsController;
import hotelManager.events.PanelSwitcherClickReceiver;

import javax.swing.*;

/**
 * Overall statistics panel
 */
public class OverallStatsPanel extends JPanel {

    private JButton backButton;
    private OverallStatsController controller;

    public OverallStatsPanel(OverallStatsController controller) {
        super();
        this.controller = controller;
        add(new JLabel("Statistiques générale"));
        initButton();
    }

    private void initButton() {
        backButton = new JButton("Retour", new ImageIcon(Icons.getBackIcon()));
        backButton.setBorderPainted(false);
        backButton.addMouseListener(new MouseClickController(new PanelSwitcherClickReceiver(controller.getAppController(), AppController.PANEL_APP_WELCOME)));
        add(backButton);
    }
}
