package hotelManager.view.panels;

import hotelManager.AppConfig;
import hotelManager.Icons;
import hotelManager.controllers.AppController;
import hotelManager.controllers.MouseClickController;
import hotelManager.controllers.WelcomeController;
import hotelManager.events.PanelSwitcherClickReceiver;
import hotelManager.model.Hotel;
import hotelManager.utils.CustomTextField;
import hotelManager.view.component.HotelList;
import hotelManager.view.component.HotelListItem;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

/**
 * Welcome panel
 *
 * @author Baudouin Feildel
 */
public class WelcomePanel extends JPanel {

    private WelcomeController controller;
    private JPanel header;
    private JPanel main;

    private JLabel hotelChainName;
    private JLabel hotelChainLogo;
    private JButton btnParameters;
    private JButton btnStats;
    private CustomTextField searchBox;
    private HotelList hotelList;

    public WelcomePanel(WelcomeController controller) {
        super();
        this.controller = controller;

        setLayout(new BorderLayout());

        initHeader();
        initMain();
    }

    private void initMain() {
        main = new JPanel();
        main.setLayout(new GridBagLayout());

        initButtons();
        initHotelList();

        add(main, BorderLayout.CENTER);
    }

    private void initHotelList() {
        searchBox = new CustomTextField(20);
        searchBox.setPlaceholder("Rechercher un hotel");
        searchBox.setToolTipText("Rechercher un hotel");
        searchBox.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {
            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {
            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {
                hotelList.setFilter(searchBox.getText());
            }
        });

        GridBagConstraints cSearchBox = new GridBagConstraints();
        cSearchBox.gridx = 1;
        cSearchBox.gridy = 0;
        cSearchBox.weightx = 1;
        cSearchBox.weighty = 0.1;
        cSearchBox.fill = GridBagConstraints.HORIZONTAL;

        main.add(searchBox, cSearchBox);

        hotelList = new HotelList(controller);

        GridBagConstraints cHotelList = new GridBagConstraints();
        cHotelList.gridx = 1;
        cHotelList.gridy = 1;
        cHotelList.weightx = 1;
        cHotelList.weighty = 1;
        cHotelList.gridheight = 2;
        cHotelList.fill = GridBagConstraints.BOTH;

        main.add(hotelList, cHotelList);
    }

    private void initButtons() {
        btnParameters = new JButton();
        btnParameters.setIcon(new ImageIcon(Icons.getParametersIcon()));
        btnParameters.setBorderPainted(false);
        btnParameters.setBackground(AppConfig.BTN_BACKGROUND_COLOR);

        btnParameters.addMouseListener(
                new MouseClickController(
                        new PanelSwitcherClickReceiver(
                                controller.getAppController(),
                                AppController.PANEL_SETTINGS
                        )
                )
        );

        GridBagConstraints cParams = new GridBagConstraints();
        cParams.gridx = 2;
        cParams.gridy = 2;
        cParams.weightx = 0.2;
        cParams.weighty = 0.1;
        cParams.anchor = GridBagConstraints.LAST_LINE_END;

        main.add(btnParameters, cParams);

        btnStats = new JButton();
        btnStats.setIcon(new ImageIcon(Icons.getStatsIcon()));
        btnStats.setBorderPainted(false);
        btnStats.setBackground(AppConfig.BTN_BACKGROUND_COLOR);

        btnStats.addMouseListener(
                new MouseClickController(
                        new PanelSwitcherClickReceiver(
                                controller.getAppController(),
                                AppController.PANEL_OVERALL_STATS)
                )
        );

        GridBagConstraints cStats = new GridBagConstraints();
        cStats.gridx = 0;
        cStats.gridy = 2;
        cStats.weightx = 0.2;
        cStats.weighty = 0.1;
        cStats.anchor = GridBagConstraints.LAST_LINE_START;

        main.add(btnStats, cStats);
    }

    /**
     * Initialize Header of Welcome panel
     */
    private void initHeader() {
        header = new JPanel();
        header.setLayout(new GridLayout());

        initHotelChainLogo();
        initHotelChainName();
        add(header, BorderLayout.NORTH);
    }

    /**
     * Initialize hotel chain name
     */
    private void initHotelChainName() {
        hotelChainName = new JLabel();
        hotelChainName.setText(AppConfig.getHotelChainName());
        hotelChainName.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 40));
        header.add(hotelChainName);
    }

    /**
     * Initialize hotel chain logo
     */
    private void initHotelChainLogo() {
        ImageIcon img = new ImageIcon(AppConfig.getHotelChainLogo());
        hotelChainLogo = new JLabel(img);
        hotelChainLogo.setSize(new Dimension(200, 200));
        header.add(hotelChainLogo);
    }

    public void addHotel(Hotel hotel) {
        hotelList.add(new HotelListItem(hotel));
    }

    public void addHotelList(ArrayList<Hotel> hotels) {
        for (Hotel h : hotels) {
            hotelList.add(new HotelListItem(h));
        }
    }

    public void clearHotelList() {
        hotelList.clear();
    }

    public void clearSearchBox() {
        searchBox.setText("");
    }
}
