package hotelManager.view.panels;

import hotelManager.controllers.AppController;
import hotelManager.controllers.NewBookingController;
import hotelManager.model.Hotel;
import hotelManager.utils.ArrowToggleButtonBarCellIcon;
import hotelManager.view.panels.booking.ClientPanel;
import hotelManager.view.panels.booking.ConfirmPanel;
import hotelManager.view.panels.booking.DatePanel;
import hotelManager.view.panels.booking.RoomPanel;

import javax.swing.*;
import java.awt.*;

/**
 * New resa panel
 */
public class NewBookingPanel extends JPanel {

    public static final String PANEL_DATE = "Date";
    public static final String PANEL_ROOM = "Sélection chambre";
    public static final String PANEL_CLIENT = "Compte client";
    public static final String PANEL_CONFIRM = "Confirmation";
    private static final Color BREADCRUMB_HOVER_COLOR = new Color(200, 200, 200);
    JPanel breadcrumb;
    AbstractButton bdDateButton;
    AbstractButton bdRoomButton;
    AbstractButton bdClientButton;
    AbstractButton bdConfirmButton;

    JPanel mainContent;
    CardLayout mainLayout;
    DatePanel panelDate;
    RoomPanel panelRoom;
    JPanel panelClient;
    JPanel panelConfirm;

    NewBookingController controller;

    public NewBookingPanel(NewBookingController controller) {
        super(new BorderLayout());
        this.controller = controller;
        initBreadcrumb();
        initContent();
    }

    private static JPanel makeBreadcrumbPanel(int overlap) {
        JPanel p = new JPanel(new FlowLayout(FlowLayout.LEADING, -overlap, 0)) {
            @Override
            public boolean isOptimizedDrawingEnabled() {
                return false;
            }
        };
        p.setBorder(BorderFactory.createEmptyBorder(4, overlap + 4, 4, 4));
        p.setOpaque(false);
        return p;
    }

    private static AbstractButton makeBreadcrumbButton(String text, Color color) {
        AbstractButton b = new JRadioButton(text) {
            @Override
            public boolean contains(int x, int y) {
                Icon i = getIcon();
                if (i instanceof ArrowToggleButtonBarCellIcon) {
                    return ((ArrowToggleButtonBarCellIcon) i).getShape().contains(x, y);
                } else {
                    return super.contains(x, y);
                }
            }
        };

        b.setIcon(new ArrowToggleButtonBarCellIcon());
        b.setContentAreaFilled(false);
        b.setBorder(BorderFactory.createEmptyBorder());
        b.setVerticalAlignment(SwingConstants.CENTER);
        b.setVerticalTextPosition(SwingConstants.CENTER);
        b.setHorizontalAlignment(SwingConstants.CENTER);
        b.setHorizontalTextPosition(SwingConstants.CENTER);
        b.setFocusPainted(false);
        b.setOpaque(false);
        b.setBackground(color);
        return b;
    }

    private void initContent() {
        mainLayout = new CardLayout();
        mainContent = new JPanel(mainLayout);

        initPanelDate();
        initPanelRoom();
        initPanelClient();
        initPanelConfirm();

        add(mainContent);
        showPanel(PANEL_DATE);
    }

    public void showPanel(String panelName) {
        bdClientButton.setSelected(false);
        bdConfirmButton.setSelected(false);
        bdDateButton.setSelected(false);
        bdRoomButton.setSelected(false);

        switch (panelName) {
            case PANEL_CLIENT:
                bdClientButton.setSelected(true);
                break;

            case PANEL_CONFIRM:
                bdConfirmButton.setSelected(true);
                break;

            case PANEL_DATE:
                bdDateButton.setSelected(true);
                break;

            case PANEL_ROOM:
                bdRoomButton.setSelected(true);
                break;
        }

        mainLayout.show(mainContent, panelName);
    }

    private void initPanelDate() {
        panelDate = new DatePanel();
        mainContent.add(panelDate, PANEL_DATE);
    }

    private void initPanelRoom() {
        panelRoom = new RoomPanel(controller);
        mainContent.add(panelRoom, PANEL_ROOM);
    }

    private void initPanelClient() {
        panelClient = new ClientPanel(controller);
        mainContent.add(panelClient, PANEL_CLIENT);
    }

    private void initPanelConfirm() {
        panelConfirm = new ConfirmPanel(controller);
        mainContent.add(panelConfirm, PANEL_CONFIRM);
    }

    private void initBreadcrumb() {
        breadcrumb = makeBreadcrumbPanel(6);
        ButtonGroup bg = new ButtonGroup();
        bdDateButton = makeBreadcrumbButton(PANEL_DATE, BREADCRUMB_HOVER_COLOR);
        bdRoomButton = makeBreadcrumbButton(PANEL_ROOM, BREADCRUMB_HOVER_COLOR);
        bdClientButton = makeBreadcrumbButton(PANEL_CLIENT, BREADCRUMB_HOVER_COLOR);
        bdConfirmButton = makeBreadcrumbButton(PANEL_CONFIRM, BREADCRUMB_HOVER_COLOR);

        bdDateButton.addActionListener(actionEvent -> showPanel(PANEL_DATE));
        bdRoomButton.addActionListener(actionEvent -> {
            controller.reloadRooms();
            showPanel(PANEL_ROOM);
        });
        bdClientButton.addActionListener(actionEvent -> {
            if (controller.getRoom() != null) {
                AppController.getInstance().showError("Veuillez sélectionner une chambre", "Avant de sélectionner un client vous devez sélectionner une chambre.");
            } else {
                showPanel(PANEL_CLIENT);
            }
        });
        bdConfirmButton.addActionListener(actionEvent -> showPanel(PANEL_CONFIRM));

        breadcrumb.add(bdDateButton);
        breadcrumb.add(bdRoomButton);
        breadcrumb.add(bdClientButton);
        breadcrumb.add(bdConfirmButton);

        bg.add(bdDateButton);
        bg.add(bdRoomButton);
        bg.add(bdClientButton);
        bg.add(bdConfirmButton);

        add(breadcrumb, BorderLayout.NORTH);
    }

    public DatePanel getPanelDate() {
        return panelDate;
    }

    public RoomPanel getPanelRoom() {
        return panelRoom;
    }

    public Hotel getHotel() {
        return panelDate.getHotel();
    }

    public void setHotel(Hotel hotel) {
        panelDate.setHotel(hotel);
        panelRoom.clearRoomList();
    }
}
