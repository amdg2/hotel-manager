package hotelManager.view.panels;

import hotelManager.Icons;
import hotelManager.controllers.AppController;
import hotelManager.controllers.EmployeeEditorController;
import hotelManager.model.*;
import hotelManager.utils.CustomSpinner;
import hotelManager.utils.CustomTextField;

import javax.swing.*;

/**
 * Employee editor panel
 */
public class EmployeeEditorPanel extends JPanel {

    private static final int LABEL_HEIGHT = 30;
    private static final int LINE_SEPARATION = 35;
    private static final int INPUT_WIDTH = 300;
    private static final int PADDING = 15;
    private Hotel hotel;
    private Employee employee;
    private SpringLayout layout;
    private JLabel labelNom;
    private JLabel labelFonction;
    private JLabel labelResponsable;
    private JLabel labelAdresse;
    private CustomTextField inputNom;
    private JComboBox<Function> inputFonction;
    private JCheckBox inputResponsable;
    private CustomTextField inputAdresse;
    private CustomSpinner inputCodePostal;
    private JButton buttonValidateCodePostal;
    private JComboBox<City> inputVille;
    private JButton buttonSave;
    private JButton buttonDelete;
    private EmployeeEditorController controller;
    private JButton buttonAdd;
    private JButton buttonNew;

    public EmployeeEditorPanel(EmployeeEditorController controller) {
        super();
        this.controller = controller;
        layout = new SpringLayout();
        setLayout(layout);

        initLabels();
        initInputs();
        initButtons();
        loadFunctions();
    }

    private void initButtons() {
        buttonNew = new JButton("Ajouter un employé", new ImageIcon(Icons.getPersonAddSmallIcon()));
        buttonAdd = new JButton("Ajouter", new ImageIcon(Icons.getPersonAddSmallIcon()));
        buttonSave = new JButton("Enregistrer", new ImageIcon(Icons.getSaveIcon()));
        buttonDelete = new JButton("Supprimer", new ImageIcon(Icons.getDeleteIcon()));

        buttonSave.addActionListener(actionEvent -> controller.saveEmployee());
        buttonAdd.addActionListener(actionEvent -> controller.addEmployee());
        buttonNew.addActionListener(actionEvent -> controller.openNewEmployeeEditor());
        buttonDelete.addActionListener(actionEvent -> {
            if (JOptionPane.showConfirmDialog(this,
                    "Voulez-vous vraiment supprimer cet employé ?",
                    "Confirmation",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE) == JOptionPane.OK_OPTION)
                controller.deleteEmployee();
        });

        add(buttonNew);
        add(buttonAdd);
        add(buttonSave);
        add(buttonDelete);

        layout.putConstraint(SpringLayout.SOUTH, buttonNew, -PADDING, SpringLayout.SOUTH, this);
        layout.putConstraint(SpringLayout.SOUTH, buttonAdd, -PADDING, SpringLayout.SOUTH, this);
        layout.putConstraint(SpringLayout.SOUTH, buttonSave, -PADDING, SpringLayout.SOUTH, this);
        layout.putConstraint(SpringLayout.SOUTH, buttonDelete, -PADDING, SpringLayout.SOUTH, this);

        layout.putConstraint(SpringLayout.WEST, buttonNew, PADDING, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.EAST, buttonAdd, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, buttonSave, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, buttonDelete, -PADDING, SpringLayout.WEST, buttonSave);
    }

    private void initLabels() {
        labelNom = new JLabel("Nom");
        labelFonction = new JLabel("Fonction");
        labelResponsable = new JLabel("Responsable");
        labelAdresse = new JLabel("Adresse");

        labelNom.setVerticalTextPosition(JLabel.CENTER);
        labelFonction.setVerticalTextPosition(JLabel.CENTER);
        labelResponsable.setVerticalTextPosition(JLabel.CENTER);
        labelAdresse.setVerticalTextPosition(JLabel.CENTER);

        add(labelNom);
        add(labelFonction);
        add(labelResponsable);
        add(labelAdresse);

        layout.putConstraint(SpringLayout.WEST, labelNom, PADDING, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, labelFonction, PADDING, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, labelResponsable, PADDING, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.WEST, labelAdresse, PADDING, SpringLayout.WEST, this);

        layout.putConstraint(SpringLayout.NORTH, labelNom, LINE_SEPARATION, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.NORTH, labelFonction, LINE_SEPARATION, SpringLayout.NORTH, labelNom);
        layout.putConstraint(SpringLayout.NORTH, labelResponsable, LINE_SEPARATION, SpringLayout.NORTH, labelFonction);
        layout.putConstraint(SpringLayout.NORTH, labelAdresse, LINE_SEPARATION, SpringLayout.NORTH, labelResponsable);

        layout.putConstraint(SpringLayout.SOUTH, labelNom, LABEL_HEIGHT, SpringLayout.NORTH, labelNom);
        layout.putConstraint(SpringLayout.SOUTH, labelFonction, LABEL_HEIGHT, SpringLayout.NORTH, labelFonction);
        layout.putConstraint(SpringLayout.SOUTH, labelResponsable, LABEL_HEIGHT, SpringLayout.NORTH, labelResponsable);
        layout.putConstraint(SpringLayout.SOUTH, labelAdresse, LABEL_HEIGHT, SpringLayout.NORTH, labelAdresse);
    }

    private void initInputs() {
        inputNom = new CustomTextField();
        inputFonction = new JComboBox<>();
        inputResponsable = new JCheckBox();
        inputAdresse = new CustomTextField();
        inputCodePostal = new CustomSpinner(new SpinnerNumberModel());
        buttonValidateCodePostal = new JButton("Valider Code Postal", new ImageIcon(Icons.getValidateIcon()));
        inputVille = new JComboBox<>();

        inputVille.setEnabled(false);

        add(inputNom);
        add(inputFonction);
        add(inputResponsable);
        add(inputAdresse);
        add(inputCodePostal);
        add(buttonValidateCodePostal);
        add(inputVille);

        layout.putConstraint(SpringLayout.EAST, inputNom, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, inputFonction, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, inputResponsable, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, inputAdresse, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, buttonValidateCodePostal, -PADDING, SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, inputCodePostal, -10, SpringLayout.EAST, buttonValidateCodePostal);
        layout.putConstraint(SpringLayout.EAST, inputVille, -PADDING, SpringLayout.EAST, this);

        layout.putConstraint(SpringLayout.NORTH, inputNom, LINE_SEPARATION, SpringLayout.NORTH, this);
        layout.putConstraint(SpringLayout.NORTH, inputFonction, LINE_SEPARATION, SpringLayout.NORTH, inputNom);
        layout.putConstraint(SpringLayout.NORTH, inputResponsable, LINE_SEPARATION + 5, SpringLayout.NORTH, inputFonction);
        layout.putConstraint(SpringLayout.NORTH, inputAdresse, LINE_SEPARATION, SpringLayout.NORTH, inputResponsable);
        layout.putConstraint(SpringLayout.NORTH, inputCodePostal, LINE_SEPARATION, SpringLayout.NORTH, inputAdresse);
        layout.putConstraint(SpringLayout.NORTH, buttonValidateCodePostal, LINE_SEPARATION, SpringLayout.NORTH, inputAdresse);
        layout.putConstraint(SpringLayout.NORTH, inputVille, LINE_SEPARATION, SpringLayout.NORTH, buttonValidateCodePostal);

        layout.putConstraint(SpringLayout.WEST, inputNom, -(PADDING + INPUT_WIDTH), SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.WEST, inputFonction, -(PADDING + INPUT_WIDTH), SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.WEST, inputAdresse, -(PADDING + INPUT_WIDTH), SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.WEST, inputVille, -(PADDING + INPUT_WIDTH), SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.WEST, inputResponsable, -(PADDING + INPUT_WIDTH), SpringLayout.EAST, this);

        layout.putConstraint(SpringLayout.WEST, inputCodePostal, -(PADDING + INPUT_WIDTH), SpringLayout.EAST, this);
        layout.putConstraint(SpringLayout.EAST, inputCodePostal, -10, SpringLayout.WEST, buttonValidateCodePostal);

        inputResponsable.setHorizontalAlignment(JCheckBox.RIGHT);

        buttonValidateCodePostal.addActionListener(actionEvent -> loadVille());
    }

    private void loadFunctions() {
        if (inputFonction.getItemCount() == 0) {
            try {
                DbManager.getInstance().getListFunction().stream().forEach(function -> inputFonction.addItem(function));
            } catch (Exception e) {
                AppController.getInstance().showError(e);
            }
        }
    }

    private void loadVille() {
        int codepostal = Integer.parseInt(inputCodePostal.getValue().toString());
        inputVille.removeAllItems();

        try {
            DbManager.getInstance().getCityByZipCode(codepostal).forEach(ville -> inputVille.addItem(ville));
            inputVille.setEnabled(inputVille.getItemCount() > 0);
        } catch (Exception e) {
            AppController.getInstance().showError(e);
        }
    }

    private void loadData() {
        inputNom.setText(employee.getName());
        inputFonction.setSelectedItem(employee.getFunction());
        inputResponsable.setSelected(employee.isResponsible());
        inputAdresse.setText(employee.getAddress());

        if (employee.getCity() != null && employee.getCity().getZipCode() != null) {
            int codePostal = Integer.parseInt(employee.getCity().getZipCode());
            inputCodePostal.setValue(codePostal);
        }

        loadVille();
        if (inputVille.getItemCount() > 0)
            inputVille.setSelectedItem(employee.getCity());
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setEmployee(Employee e) {
        if (employee == e)
            return;

        employee = e;
        hotel = employee.getHotel();

        loadData();
    }

    public String getNom() {
        return inputNom.getText();
    }

    public void setNom(String nom) {
        inputNom.setText(nom);
    }

    public Function getFunction() {
        return (Function) inputFonction.getSelectedItem();
    }

    public void setFunction(Function f) {
        inputFonction.setSelectedItem(f);
    }

    public boolean getEstResponsable() {
        return inputResponsable.isSelected();
    }

    public void setEstResponsable(boolean b) {
        inputResponsable.setSelected(b);
    }

    public String getAdresse() {
        return inputAdresse.getText();
    }

    public void setAdresse(String addr) {
        inputAdresse.setText(addr);
    }

    public String getCodePostal() {
        return inputCodePostal.getValue().toString();
    }

    public City getVille() {
        return (City) inputVille.getSelectedItem();
    }

    public void setVille(City c) {
        inputCodePostal.setValue(Integer.parseInt(c.getZipCode()));
        loadVille();
        inputVille.setSelectedItem(c);
    }

    public void switchToAddEmployee() {
        buttonAdd.setVisible(true);
        buttonDelete.setVisible(false);
        buttonSave.setVisible(false);
        buttonNew.setVisible(false);
    }

    public void switchToEditEmployee() {
        buttonAdd.setVisible(false);
        buttonDelete.setVisible(true);
        buttonSave.setVisible(true);
        buttonNew.setVisible(true);
    }

    public void clear() {
        inputNom.setText("");
        inputFonction.setSelectedIndex(-1);
        inputResponsable.setSelected(false);
        inputAdresse.setText("");
        inputCodePostal.setValue(0);
        loadVille();
    }
}
