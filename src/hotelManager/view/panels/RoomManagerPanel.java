package hotelManager.view.panels;

import hotelManager.Icons;
import hotelManager.controllers.RoomEditorController;
import hotelManager.controllers.RoomManagerController;
import hotelManager.model.Hotel;
import hotelManager.model.Room;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import java.awt.*;

/**
 * Room manager panel
 */
public class RoomManagerPanel extends JPanel {

    private static final String ROOM_EDITOR = "Room Editor";
    private static final String ROOM_MANAGER_HOME = "Room Manager Home";


    private JList<Room> roomList;
    private Hotel hotel = null;

    private RoomManagerController controller;
    private RoomEditorController roomEditor;
    private JPanel contentSwitcher;
    private JPanel managerHome;
    private CardLayout contentSwitcherLayout;

    public RoomManagerPanel(RoomManagerController controller) {
        super();
        this.controller = controller;
        setLayout(new BorderLayout());
        initPanelHeader();
        initPanelContent();
    }

    private void initPanelHeader() {
        JPanel headerPanel = new JPanel(new BorderLayout());
        JLabel panelTitle = new JLabel("Gestion des chambres");

        panelTitle.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 40));
        panelTitle.setHorizontalAlignment(SwingConstants.CENTER);

        headerPanel.add(panelTitle, BorderLayout.CENTER);
        add(headerPanel, BorderLayout.NORTH);
    }

    private void initPanelContent() {
        roomList = new JList<>();
        roomList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        roomList.addListSelectionListener(this::roomSelected);

        JScrollPane scrollRoom = new JScrollPane();
        scrollRoom.setPreferredSize(new Dimension(200, 200));
        scrollRoom.setViewportView(roomList);
        add(scrollRoom, BorderLayout.WEST);

        contentSwitcher = new JPanel(new CardLayout());
        contentSwitcherLayout = (CardLayout) contentSwitcher.getLayout();

        roomEditor = new RoomEditorController(controller);
        initManagerHome();

        contentSwitcher.add(roomEditor.getPanel(), ROOM_EDITOR);
        contentSwitcher.add(managerHome, ROOM_MANAGER_HOME);

        add(contentSwitcher, BorderLayout.CENTER);
        contentSwitcherLayout.show(contentSwitcher, ROOM_MANAGER_HOME);
    }

    private void roomSelected(ListSelectionEvent event) {
        if (!roomList.isSelectionEmpty()) {
            selectRoom(roomList.getSelectedValue());
        }
    }
    public void selectRoom(Room r) {
        if (r != null) {
            roomEditor.setRoom(r);
            showRoomEditor();
        }
    }


    private void initManagerHome() {
        managerHome = new JPanel(new BorderLayout());

        JLabel text = new JLabel("<html><div style='text-align: center;'>Veuillez sélectionner une chambre pour voir et éditer ses informations.<br>Ou utilisez le bouton ci-dessous pour ajouter une chambre.</div></html>");
        text.setVerticalTextPosition(JLabel.CENTER);
        text.setHorizontalAlignment(JLabel.CENTER);
        managerHome.add(text, BorderLayout.CENTER);

        JButton button = new JButton("Ajouter une chambre", new ImageIcon(Icons.getPersonAddIcon()));
        button.addActionListener(actionEvent -> showNewRoomEditor());
        managerHome.add(button, BorderLayout.SOUTH);
    }

    private void updateData() {
        showPanel(ROOM_MANAGER_HOME);
    }

    public void setRoomList(Room[] rooms) {
        roomList.setListData(rooms);
    }

    public RoomEditorController getEditorController() {
        return roomEditor;
    }

    public void showManagerHome() {
        showPanel(ROOM_MANAGER_HOME);
    }

    public void showRoomEditor() {
        roomEditor.openEditRoomEditor();
        showPanel(ROOM_EDITOR);
    }

    public void showNewRoomEditor() {
        roomEditor.openNewRoomEditor();
        showPanel(ROOM_EDITOR);
    }

    private void showPanel(String panelName) {
        contentSwitcherLayout.show(contentSwitcher, panelName);
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        if (hotel == this.hotel)
            return;

        this.hotel = hotel;
        updateData();
    }
    public JList<Room> getListRoom() {
        return roomList;
    }
}
