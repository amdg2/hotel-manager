package hotelManager.controllers;

import hotelManager.model.*;
import hotelManager.view.panels.NewBookingPanel;
import hotelManager.view.panels.ResaManagerPanel;

import javax.swing.*;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Booking controller
 */
public class NewBookingController extends BaseController {

    private ResaManagerController parentController;
    private Room bookingRoom;
    private Customer customer;
    private Object room;

    public NewBookingController(AppController app, ResaManagerController parentController) {
        super(app);
        this.parentController = parentController;
        setPanel(new NewBookingPanel(this));
    }

    @Override
    public NewBookingPanel getPanel() {
        return (NewBookingPanel) super.getPanel();
    }

    @Override
    public void onOpen() {
        getPanel().setHotel(getAppController().getCurrentHotel());
        getPanel().getPanelDate().setStartDate(LocalDateTime.now(), false);
        getPanel().getPanelDate().setEndDate(LocalDateTime.now().plusDays(5));
        getPanel().getPanelDate().setBedNumber(2);
        getPanel().showPanel(NewBookingPanel.PANEL_DATE);
    }

    public void reloadRooms() {
        Hotel h = getPanel().getHotel();
        LocalDateTime startDate = getPanel().getPanelDate().getStartDate();
        LocalDateTime endDate = getPanel().getPanelDate().getEndDate();

        List<Room> filteredRooms = h.getRooms().stream().filter(room -> {
            try {
                List<Reservation> resa = DbManager.getInstance().getListReservationRoom(room);
                boolean ok = true;

                for (Reservation r : resa) {
                    Date ED = new Date(r.getEndDate().getTime());
                    Date SD = new Date(r.getStartDate().getTime());
                    LocalDateTime rED = LocalDateTime.ofInstant(ED.toInstant(), ZoneId.systemDefault());
                    LocalDateTime rSD = LocalDateTime.ofInstant(SD.toInstant(), ZoneId.systemDefault());

                    if (rED.isAfter(startDate) && endDate.isAfter(rED) ||
                            startDate.isAfter(rSD) && endDate.isBefore(rED) ||
                            startDate.isBefore(rSD) && endDate.isAfter(rSD) ||
                            startDate.equals(rSD) || endDate.equals(rED))
                        ok = false;
                }

                return ok;
            } catch (Exception e) {
                e.printStackTrace();
                AppController.getInstance().showError(e);
                return true;
            }
        }).collect(Collectors.toList());

        getPanel().getPanelRoom().clearRoomList();
        getPanel().getPanelRoom().addRoomList(filteredRooms);
    }

    public void selectRoom(Room room) {
        bookingRoom = room;
        getPanel().showPanel(NewBookingPanel.PANEL_CLIENT);
    }

    public void selectClient(Customer client) {
        customer = client;
        getPanel().showPanel(NewBookingPanel.PANEL_CONFIRM);
    }

    public Room getRoom() {
        return bookingRoom;
    }

    public void book() {
        try {
            Date SD = Date.from(getPanel().getPanelDate().getStartDate().atZone(ZoneId.systemDefault()).toInstant());
            Date ED = Date.from(getPanel().getPanelDate().getEndDate().atZone(ZoneId.systemDefault()).toInstant());
            DbManager.getInstance().addReservation(customer, bookingRoom, SD, ED);
            JOptionPane.showMessageDialog(AppController.getInstance().getMainWindow(), "Réservation ajouté avec succès !", "Réussite", JOptionPane.INFORMATION_MESSAGE);
            getPanel().showPanel(NewBookingPanel.PANEL_DATE);
            parentController.getPanel().showPanel(ResaManagerPanel.RESA_MANAGER);
        } catch (Exception e) {
            e.printStackTrace();
            AppController.getInstance().showError(e);
        }
    }
}
