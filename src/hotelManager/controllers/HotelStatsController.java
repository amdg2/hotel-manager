package hotelManager.controllers;

import hotelManager.view.panels.HotelStatsPanel;

/**
 * Hotel statistics controller
 */
public class HotelStatsController extends BaseController {

    public HotelStatsController(AppController app) {
        super(app);
        setPanel(new HotelStatsPanel(this));
    }

    @Override
    public void onOpen() {

    }

    public HotelStatsPanel getPanel() {
        return (HotelStatsPanel) super.getPanel();
    }
}
