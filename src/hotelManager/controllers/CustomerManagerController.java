package hotelManager.controllers;

import hotelManager.model.Customer;
import hotelManager.model.Hotel;
import hotelManager.view.panels.CustomerManagerPanel;

import java.util.List;

/**
 * Customer manager controller
 *
 * @author Kevin
 */
public class CustomerManagerController extends BaseController{

    private Hotel hotel;

    public CustomerManagerController(AppController app) {
        super(app);
        setPanel(new CustomerManagerPanel(this));
    }

    @Override
    public void onOpen() {
        hotel = getAppController().getCurrentHotel();
        getPanel().setHotel(hotel);
        refreshCustomerList();
    }

    public CustomerManagerPanel getPanel() {
        return (CustomerManagerPanel) super.getPanel();
    }

    public void onDeleteCustomer() {
        refreshCustomerList();
        getPanel().showManagerHome();
    }

    public void onCustomerSave() {
        refreshCustomerList();
    }

    public void refreshCustomerList() {
        List<Customer> customers = hotel.getCustomers();
        Customer[] eArray = new Customer[customers.size()];
        hotel.getCustomers().toArray(eArray);
        getPanel().setCustomerList(eArray);
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void onAddCustomer(Customer e) {
        refreshCustomerList();
        int customerListSize = getPanel().getListCustomer().getModel().getSize();
        int index = 0;

        for (int i = 0; i < customerListSize; i++) {
            Customer tmp = getPanel().getListCustomer().getModel().getElementAt(i);
            if (tmp.getId() == e.getId()) {
                index = i;
                break;
            }
        }

        getPanel().getListCustomer().setSelectedIndex(index);
    }
}
