package hotelManager.controllers;

import hotelManager.view.panels.SettingsPanel;

/**
 * Settings controller
 */
public class SettingsController extends BaseController {

    public SettingsController(AppController app) {
        super(app);
        setPanel(new SettingsPanel(this));
    }

    @Override
    public void onOpen() {
        getPanel().loadData();

    }

    public SettingsPanel getPanel() {
        return (SettingsPanel) super.getPanel();
    }
}
