package hotelManager.controllers;

import hotelManager.model.DbManager;
import hotelManager.model.Hotel;
import hotelManager.view.panels.WelcomePanel;

import java.util.ArrayList;

/**
 * Welcome controller class
 */
public class WelcomeController extends BaseController {

    private ArrayList<Hotel> hotels;

    public WelcomeController(AppController app) {
        super(app);
        setPanel(new WelcomePanel(this));
    }

    @Override
    public void onOpen() {
        loadHotelList();
        getPanel().clearHotelList();
        getPanel().addHotelList(hotels);
        getPanel().clearSearchBox();
    }

    private void loadHotelList() {
        if (hotels == null) {
            try {
                hotels = DbManager.getInstance().getListHotel();
            } catch (Exception ex) {
                getAppController().showError(ex);
            }
        }
    }

    public void openHotel(Hotel hotel) {
        if (hotel == null) {
            getAppController().showError("Hotel is null.", "Hotel can't be open, because it doesn't have value.");
            return;
        }

        getAppController().setCurrentHotel(hotel);
        getAppController().openPanel(AppController.PANEL_HOTEL_WELCOME);
    }

    public WelcomePanel getPanel() {
        return (WelcomePanel) super.getPanel();
    }

    public ArrayList<Hotel> getHotels() {
        return hotels;
    }

    public void setHotels(ArrayList<Hotel> hotels) {
        this.hotels = hotels;
    }
}
