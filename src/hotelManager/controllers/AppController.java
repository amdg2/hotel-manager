package hotelManager.controllers;

import hotelManager.model.DbManager;
import hotelManager.model.Hotel;
import hotelManager.utils.OSValidator;
import hotelManager.view.MainWindow;
import hotelManager.view.panels.LeftMenu;

import javax.swing.*;

/**
 * App controller class
 */
public class AppController {

    public static final int PANEL_APP_WELCOME = 1;
    public static final int PANEL_HOTEL_WELCOME = 2;
    public static final int PANEL_EMPLOYEE_MANAGER = 3;
    public static final int PANEL_ROOM_MANAGER = 4;
    public static final int PANEL_RESA_MANAGER = 5;
    public static final int PANEL_HOTEL_STATS = 6;
    public static final int PANEL_OVERALL_STATS = 7;
    public static final int PANEL_SETTINGS = 8;
    public static final int PANEL_CUSTOMER_MANAGER = 9;
    private static AppController instance;
    private MainWindow mainWindow;
    private LeftMenu leftMenu;
    private WelcomeController welcomeController;
    private HotelDashboardController dashboardController;
    private Hotel currentHotel;
    private EmployeeManagerController employeeManagerController;
    private HotelStatsController hotelStatsController;
    private RoomManagerController roomManagerController;
    private ResaManagerController resaManagerController;
    private OverallStatsController overallStatsController;
    private SettingsController settingsController;
    private CustomerManagerController customerManagerController;

    private AppController() {
        setExceptionHandler();
        initLookAndFeel();
        initControllers();
        mainWindow = new MainWindow(this);
        openPanel(PANEL_APP_WELCOME);
        mainWindow.pack();
        mainWindow.setVisible(true);
    }

    public static AppController getInstance() {
        if (instance == null)
            instance = new AppController();

        return instance;
    }

    private void initControllers() {
        welcomeController = new WelcomeController(this);
        dashboardController = new HotelDashboardController(this);
        employeeManagerController = new EmployeeManagerController(this);
        hotelStatsController = new HotelStatsController(this);
        roomManagerController = new RoomManagerController(this);
        resaManagerController = new ResaManagerController(this);
        overallStatsController = new OverallStatsController(this);
        settingsController = new SettingsController(this);
        customerManagerController = new CustomerManagerController(this);

        leftMenu = new LeftMenu(this);
    }

    private void initLookAndFeel() {
        try {
            if (OSValidator.isWindows())
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            else if (OSValidator.isLinux())
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
            else if (OSValidator.isUnix())
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
            else
                UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | UnsupportedLookAndFeelException | IllegalAccessException e) {
            showError(e);
        }
    }

    private void setExceptionHandler() {
        Thread.currentThread().setUncaughtExceptionHandler((thread, throwable) -> {
            throwable.printStackTrace();
            showError("Exception non gérée", throwable.toString());
        });
    }

    /**
     * Show exception dialog
     *
     * @param exception unhandled exception
     */
    public void showError(Exception exception) {
        exception.printStackTrace();
        JOptionPane.showMessageDialog(mainWindow,
                exception.toString() + " at : " + exception.getStackTrace()[0].toString(),
                "Exception",
                JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Show error message
     *
     * @param title       Message title
     * @param description Message description
     */
    public void showError(String title, String description) {
        JOptionPane.showMessageDialog(mainWindow,
                description,
                title,
                JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Get the current hotel
     * @return the current hotel
     */
    public Hotel getCurrentHotel() {
        return currentHotel;
    }

    /**
     * Set current Hotel
     * @param currentHotel new current hotel
     */
    public void setCurrentHotel(Hotel currentHotel) {
        this.currentHotel = currentHotel;
    }

    /**
     * Open a panel
     * @param panel Panel to open
     */
    public void openPanel(int panel) {
        switch (panel) {
            case PANEL_APP_WELCOME:
                welcomeController.onOpen();
                mainWindow.setLeftPanel(null);
                mainWindow.setMainPanel(welcomeController.getPanel());
                break;

            case PANEL_HOTEL_WELCOME:
                dashboardController.onOpen();
                mainWindow.setLeftPanel(leftMenu);
                mainWindow.setMainPanel(dashboardController.getPanel());
                leftMenu.showBackToHotelListButton();
                break;

            case PANEL_EMPLOYEE_MANAGER:
                employeeManagerController.onOpen();
                mainWindow.setLeftPanel(leftMenu);
                mainWindow.setMainPanel(employeeManagerController.getPanel());
                leftMenu.showBackToHotelDashboardButton();
                break;

            case PANEL_HOTEL_STATS:
                hotelStatsController.onOpen();
                mainWindow.setLeftPanel(leftMenu);
                mainWindow.setMainPanel(hotelStatsController.getPanel());
                leftMenu.showBackToHotelDashboardButton();
                break;

            case PANEL_ROOM_MANAGER:
                roomManagerController.onOpen();
                mainWindow.setLeftPanel(leftMenu);
                mainWindow.setMainPanel(roomManagerController.getPanel());
                leftMenu.showBackToHotelDashboardButton();
                break;

            case PANEL_RESA_MANAGER:
                resaManagerController.onOpen();
                mainWindow.setLeftPanel(leftMenu);
                mainWindow.setMainPanel(resaManagerController.getPanel());
                leftMenu.showBackToHotelDashboardButton();
                break;

            case PANEL_OVERALL_STATS:
                overallStatsController.onOpen();
                mainWindow.setLeftPanel(null);
                mainWindow.setMainPanel(overallStatsController.getPanel());
                break;

            case PANEL_SETTINGS:
                settingsController.onOpen();
                mainWindow.setLeftPanel(null);
                mainWindow.setMainPanel(settingsController.getPanel());
                break;

            case PANEL_CUSTOMER_MANAGER:
                customerManagerController.onOpen();
                mainWindow.setLeftPanel(leftMenu);
                mainWindow.setMainPanel(customerManagerController.getPanel());
                leftMenu.showBackToHotelDashboardButton();
                break;
        }
    }

    /**
     * Get Main Window instance
     * @return The main window instance
     */
    public MainWindow getMainWindow() {
        return mainWindow;
    }

    /**
     * Clean resources before exit application
     */
    public void onExitApplication() {
        try {
            DbManager.getInstance().closeSqlConnection();
        } catch (Exception e) {
            AppController.getInstance().showError(e);
        }
    }
}
