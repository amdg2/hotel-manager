package hotelManager.controllers;

import hotelManager.view.panels.ResaManagerPanel;

/**
 * Reservation manager controller
 */
public class ResaManagerController extends BaseController {

    private NewBookingController newBookingController;

    public ResaManagerController(AppController app) {
        super(app);
        // Initialize controllers
        newBookingController = new NewBookingController(app, this);

        // Initialize panels
        setPanel(new ResaManagerPanel(this));
    }

    @Override
    public void onOpen() {
        newBookingController.onOpen();
    }

    public ResaManagerPanel getPanel() {
        return (ResaManagerPanel) super.getPanel();
    }


    public NewBookingController getNewBookingController() {
        return newBookingController;
    }

    public void setNewBookingController(NewBookingController newBookingController) {
        this.newBookingController = newBookingController;
    }
}
