package hotelManager.controllers;

import hotelManager.view.panels.HotelDashboardPanel;

/**
 * Hotel dashboard controller
 */
public class HotelDashboardController extends BaseController {

    public HotelDashboardController(AppController app) {
        super(app, new HotelDashboardPanel());
    }

    @Override
    public void onOpen() {
        getPanel().setHotel(getAppController().getCurrentHotel());
    }

    public HotelDashboardPanel getPanel() {
        return (HotelDashboardPanel) super.getPanel();
    }
}
