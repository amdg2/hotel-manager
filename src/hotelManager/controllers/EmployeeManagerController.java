package hotelManager.controllers;

import hotelManager.model.Employee;
import hotelManager.model.Hotel;
import hotelManager.view.panels.EmployeeManagerPanel;

import java.util.List;

/**
 * Employee manager controller
 */
public class EmployeeManagerController extends BaseController {

    private Hotel hotel;

    public EmployeeManagerController(AppController app) {
        super(app);
        setPanel(new EmployeeManagerPanel(this));
    }

    @Override
    public void onOpen() {
        hotel = getAppController().getCurrentHotel();
        getPanel().setHotel(hotel);
        refreshEmployeeList();
    }

    public EmployeeManagerPanel getPanel() {
        return (EmployeeManagerPanel) super.getPanel();
    }

    public void onDeleteEmployee() {
        refreshEmployeeList();
        getPanel().showManagerHome();
    }

    public void onEmployeeSave() {
        refreshEmployeeList();
    }

    public void refreshEmployeeList() {
        List<Employee> employees = hotel.getEmployees();
        Employee[] eArray = new Employee[employees.size()];
        hotel.getEmployees().toArray(eArray);
        getPanel().setEmployeeList(eArray);
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void onAddEmployee(Employee e) {
        refreshEmployeeList();
        int employeeListSize = getPanel().getListEmployee().getModel().getSize();
        int index = 0;

        for (int i = 0; i < employeeListSize; i++) {
            Employee tmp = getPanel().getListEmployee().getModel().getElementAt(i);
            if (tmp.getId() == e.getId()) {
                index = i;
                break;
            }
        }

        getPanel().getListEmployee().setSelectedIndex(index);
    }
}
