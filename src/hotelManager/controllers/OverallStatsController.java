package hotelManager.controllers;

import hotelManager.view.panels.OverallStatsPanel;

/**
 * Overall statistics controller
 */
public class OverallStatsController extends BaseController {

    public OverallStatsController(AppController app) {
        super(app);
        setPanel(new OverallStatsPanel(this));
    }

    @Override
    public void onOpen() {

    }

    public OverallStatsPanel getPanel() {
        return (OverallStatsPanel) super.getPanel();
    }
}
