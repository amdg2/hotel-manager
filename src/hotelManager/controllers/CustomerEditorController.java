package hotelManager.controllers;

import hotelManager.model.City;
import hotelManager.model.Customer;
import hotelManager.view.panels.CustomerEditorPanel;

import java.util.Date;

/**
 * Customer Editor Controller
 *
 * @author Kevin
 */
public class CustomerEditorController {

    private CustomerManagerController parentController;
    private CustomerEditorPanel panel;

    private Customer customer;

    public CustomerEditorController(CustomerManagerController parentController) {
        panel = new CustomerEditorPanel(this);
        this.parentController = parentController;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
        panel.setCustomer(customer);
    }

    public CustomerEditorPanel getPanel() {
        return panel;
    }

    public void saveCustomer() {
        boolean needSave = false;
        String nom = panel.getNom();
        Date dateOfBirth = panel.getDateNaissance();
        String adresse = panel.getAdresse();
        String codePostal = panel.getCodePostal();
        City ville = panel.getVille();

        if (!customer.getName().equals(nom)) {
            customer.setName(nom);
            needSave = true;
        }

        if (customer.getDateOfBirth() != dateOfBirth) {
            customer.setDateOfBirth(dateOfBirth);
            needSave = true;
        }

        if (!customer.getAddress().equals(adresse)) {
            customer.setAddress(adresse);
            needSave = true;
        }

        if (!customer.getCity().getZipCode().equals(codePostal) || customer.getCity() != ville) {
            customer.setCity(ville);
            needSave = true;
        }

        if (needSave) {
            customer.save();
            parentController.onCustomerSave();
        }
    }

    public void deleteCustomer() {
        customer.delete();
        parentController.onDeleteCustomer();
    }

    public void addCustomer() {
        Customer customer = new Customer();
        customer.setNewRecord(true);
        customer.setName(panel.getNom());
        customer.setDateOfBirth(panel.getDateNaissance());
        customer.setCustomerNumber(panel.getNumClient());
        customer.setAddress(panel.getAdresse());
        customer.setCity(panel.getVille());
        customer.save();
        parentController.onAddCustomer(customer);
    }

    public void openNewCustomerEditor() {
        panel.clear();
        panel.switchToAddCustomer();
    }

    public void openEditCustomerEditor() {
        panel.setCustomer(customer);
        panel.switchToEditCustomer();
    }
}
