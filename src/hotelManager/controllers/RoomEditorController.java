package hotelManager.controllers;

import hotelManager.model.Room;
import hotelManager.view.panels.RoomEditorPanel;

/**
 * Room Editor Controller
 */
public class RoomEditorController {
    private RoomManagerController parentController;
    private RoomEditorPanel panel;

    private Room room;

    public RoomEditorController(RoomManagerController parentController) {
        panel = new RoomEditorPanel(this);
        this.parentController = parentController;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
        panel.setRoom(room);
    }

    public RoomEditorPanel getPanel() {
        return panel;
    }

    public void saveRoom() {

        boolean needSave = false;
        int number = panel.getNumber();
        int nbDoubleBed = panel.getNbDoubleBed();
        int nbSimpleBed = panel.getNbSimpleBed();
        float price = panel.getPrice();
        String statut = panel.getStatut();

        if (room.getNumber()!= number) {
            room.setNumber(number);
            needSave = true;
        }

        if (room.getNbDoubleBed() != nbDoubleBed) {
            room.setNbDoubleBed(nbDoubleBed);
            needSave = true;
        }

        if (room.getNbSimpleBed() != nbSimpleBed) {
            room.setNbSimpleBed(nbSimpleBed);
            needSave = true;
        }

        if (room.getPrice()!= price) {
            room.setPrice(price);
            needSave = true;
        }

        if (!room.getStatut().equals(statut)) {
            room.setStatut(statut);
            needSave = true;
        }

        if (needSave) {
            room.save();
            parentController.onRoomSave();
        }

    }

    public void deleteRoom() {
        room.delete();
        parentController.onDeleteRoom();

    }

    public void addRoom() {
        Room room = new Room();
        room.setNewRecord(true);
        room.setNumber(panel.getNumber());
        room.setNbDoubleBed(panel.getNbDoubleBed());
        room.setNbSimpleBed(panel.getNbSimpleBed());
        room.setPrice(panel.getPrice());
        room.setStatut(panel.getStatut());
        room.setHotel(parentController.getHotel());
        room.save();
        parentController.onAddRoom(room);
    }

    public void openNewRoomEditor() {
        panel.clear();
        panel.switchToAddRoom();

    }

    public void openEditRoomEditor() {
        panel.setRoom(room);
        panel.switchToEditRoom();
    }
}
