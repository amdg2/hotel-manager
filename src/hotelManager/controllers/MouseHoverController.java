package hotelManager.controllers;

import hotelManager.events.IMouseHoverEventReceiver;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Mouse Hover controller
 */
public class MouseHoverController implements MouseListener {

    private IMouseHoverEventReceiver receiver;

    public MouseHoverController(IMouseHoverEventReceiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {

    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {
        receiver.OnMouseEnter();
        receiver.OnMouseHover(true);
    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {
        receiver.OnMouseLeave();
        receiver.OnMouseHover(false);
    }
}
