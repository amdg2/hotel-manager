package hotelManager.controllers;

import hotelManager.events.IMouseClickEventReceiver;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Mouse click controller
 */
public class MouseClickController implements MouseListener {

    private IMouseClickEventReceiver receiver;

    public MouseClickController(IMouseClickEventReceiver receiver) {
        this.receiver = receiver;
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        switch (mouseEvent.getButton()) {
            case MouseEvent.BUTTON1:
                receiver.onClick();
                break;

            case MouseEvent.BUTTON2:
                receiver.onRightClick();
                break;
        }
    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
