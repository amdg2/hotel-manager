package hotelManager.controllers;

import hotelManager.model.City;
import hotelManager.model.Employee;
import hotelManager.model.Function;
import hotelManager.view.panels.EmployeeEditorPanel;

/**
 * Employee editor controller
 */
public class EmployeeEditorController {

    private EmployeeManagerController parentController;
    private EmployeeEditorPanel panel;

    private Employee employee;

    public EmployeeEditorController(EmployeeManagerController parentController) {
        panel = new EmployeeEditorPanel(this);
        this.parentController = parentController;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
        panel.setEmployee(employee);
    }

    public EmployeeEditorPanel getPanel() {
        return panel;
    }

    public void saveEmployee() {
        boolean needSave = false;
        String nom = panel.getNom();
        Function function = panel.getFunction();
        boolean estResponsable = panel.getEstResponsable();
        String adresse = panel.getAdresse();
        String codePostal = panel.getCodePostal();
        City ville = panel.getVille();

        if (!employee.getName().equals(nom)) {
            employee.setName(nom);
            needSave = true;
        }

        if (employee.getFunction() != function) {
            employee.setFunction(function);
            needSave = true;
        }

        if (employee.isResponsible() != estResponsable) {
            employee.setResponsible(estResponsable);
            needSave = true;
        }

        if (!employee.getAddress().equals(adresse)) {
            employee.setAddress(adresse);
            needSave = true;
        }

        if (!employee.getCity().getZipCode().equals(codePostal) || employee.getCity() != ville) {
            employee.setCity(ville);
            needSave = true;
        }

        if (needSave) {
            employee.save();
            parentController.onEmployeeSave();
        }
    }

    public void deleteEmployee() {
        employee.delete();
        parentController.onDeleteEmployee();
    }

    public void addEmployee() {
        Employee employee = new Employee();
        employee.setNewRecord(true);
        employee.setName(panel.getNom());
        employee.setFunction(panel.getFunction());
        employee.setResponsible(panel.getEstResponsable());
        employee.setAddress(panel.getAdresse());
        employee.setCity(panel.getVille());
        employee.setHotel(parentController.getHotel());
        employee.save();
        parentController.onAddEmployee(employee);
    }

    public void openNewEmployeeEditor() {
        panel.clear();
        panel.switchToAddEmployee();
    }

    public void openEditEmployeeEditor() {
        panel.setEmployee(employee);
        panel.switchToEditEmployee();
    }
}
