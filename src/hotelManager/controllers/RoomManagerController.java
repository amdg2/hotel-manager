package hotelManager.controllers;

import hotelManager.model.Hotel;
import hotelManager.model.Room;
import hotelManager.view.panels.RoomManagerPanel;

import java.util.List;

/**
 * Room manager controller
 */
public class RoomManagerController extends BaseController {

    private Hotel hotel;


    public RoomManagerController(AppController app) {
        super(app);
        setPanel(new RoomManagerPanel(this));
    }

    @Override
    public void onOpen() {
        hotel = getAppController().getCurrentHotel();
        getPanel().setHotel(hotel);
        refreshRoomList();
    }

    public RoomManagerPanel getPanel() {
        return (RoomManagerPanel) super.getPanel();
    }


    public void onDeleteRoom() {
        refreshRoomList();
        getPanel().showManagerHome();
    }

    public void onRoomSave() {
        refreshRoomList();
    }

    public void refreshRoomList() {
        List<Room> room = hotel.getRooms();
        Room[] eArray = new Room[room.size()];
        hotel.getRooms().toArray(eArray);
        getPanel().setRoomList(eArray);
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void onAddRoom(Room r) {
        refreshRoomList();
        int roomListSize = getPanel().getListRoom().getModel().getSize();
        int index = 0;

        for (int i = 0; i < roomListSize; i++) {
            Room tmp = getPanel().getListRoom().getModel().getElementAt(i);
            if (tmp.getId() == r.getId()) {
                index = i;
                break;
            }
        }

        getPanel().getListRoom().setSelectedIndex(index);
    }
}
