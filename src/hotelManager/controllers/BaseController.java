package hotelManager.controllers;

import javax.swing.*;

/**
 * BaseController class
 */
public abstract class BaseController {

    private AppController app;
    private JPanel panel;

    public BaseController(AppController app) {
        this.app = app;
    }

    public BaseController(AppController app, JPanel panel) {
        this.app = app;
        this.panel = panel;
    }

    public abstract void onOpen();

    public AppController getAppController() {
        return app;
    }

    public JPanel getPanel() {
        return panel;
    }

    protected void setPanel(JPanel panel) {
        this.panel = panel;
    }
}
