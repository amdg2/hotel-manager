package hotelManager;

import java.io.*;
import java.util.Properties;

/**
 * Property class to read a properties file
 *
 * @author kevin
 */
public class PropertyLoader {

    private static Properties properties;

    private PropertyLoader(){

    }

    public static Properties initProperties(String filename){
        FileInputStream in;
        Properties properties = new Properties();
        try {
            in = new FileInputStream(filename);
            properties.load(in);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return properties;
    }

    public static Properties getProperties() {
        if (properties == null){
            properties = initProperties(AppConfig.APPLICATION_PROPERTIES);
        }
        return properties;
    }

    public static void writeProperties(Properties properties){

        Properties jdbcProperties = new Properties();
        Properties uiProperties = new Properties();
        Properties iconProperties = new Properties();
        Properties otherProperties = new Properties();

        for (String key:properties.stringPropertyNames()){
            if (key.matches("jdbc[A-z.]+")){
                jdbcProperties.setProperty(key,properties.getProperty(key));
            }else if (key.matches("hotel[A-z.]+")){
                uiProperties.setProperty(key,properties.getProperty(key));
            }else if (key.matches("icon[A-z.]+")){
                iconProperties.setProperty(key,properties.getProperty(key));
            }else {
                otherProperties.setProperty(key,properties.getProperty(key));
            }

        }
        try {
            OutputStream out  = new FileOutputStream(AppConfig.APPLICATION_PROPERTIES);
            out.write(AppConfig.APPLICATION_PROPERTIES_HEADER.getBytes());
            out.flush();

            //Write UI Parameters
            out.write(AppConfig.APPLICATION_PROPERTIES_TITLE_UI.getBytes());
            out.flush();
            uiProperties.store(out,null);

            //Write jdbc Parameters
            out.write(AppConfig.APPLICATION_PROPERTIES_TITLE_DB.getBytes());
            out.flush();
            jdbcProperties.store(out,null);

            //Write icon Parameters
            out.write(AppConfig.APPLICATION_PROPERTIES_TITLE_ICON.getBytes());
            out.flush();
            iconProperties.store(out,null);

            //Write other Parameters

            out.write(AppConfig.APPLICATION_PROPERTIES_TITLE_OTHER.getBytes());
            out.flush();
            otherProperties.store(out,null);

            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
