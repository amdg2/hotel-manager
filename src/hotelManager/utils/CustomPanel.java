package hotelManager.utils;

import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.border.DropShadowBorder;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import java.awt.*;

/**
 * CustomPanel class (with DropShadow)
 */
public class CustomPanel extends JXPanel {

    private DropShadowBorder dropShadow;
    private Border currentBorder;

    private boolean useShadow = false;

    public CustomPanel() {
        super();
        initDropShadow();
    }

    private void initDropShadow() {
        if (dropShadow == null) {
            dropShadow = new DropShadowBorder();
            dropShadow.setShadowColor(Color.BLACK);
            dropShadow.setShowLeftShadow(true);
            dropShadow.setShowRightShadow(true);
            dropShadow.setShowBottomShadow(true);
            dropShadow.setShowTopShadow(true);
        }
    }

    public void showDropShadow() {
        if (useShadow) {
            currentBorder = getBorder();

            if (currentBorder instanceof CompoundBorder) {
                CompoundBorder compoundBorder = (CompoundBorder) currentBorder;
                super.setBorder(BorderFactory.createCompoundBorder(
                        dropShadow,
                        compoundBorder.getInsideBorder()
                ));
            } else {
                super.setBorder(BorderFactory.createCompoundBorder(dropShadow, currentBorder));
            }
        }
    }

    public void hideDropShadow() {
        if (useShadow) {
            setBorder(currentBorder);
        }
    }

    public boolean getUseShadow() {
        return useShadow;
    }

    public void setUseShadow(boolean useShadow) {
        this.useShadow = useShadow;
    }

}
