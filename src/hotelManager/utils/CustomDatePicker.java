package hotelManager.utils;

import org.jdesktop.swingx.JXDatePicker;

import java.awt.*;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * Custom date picker with white background
 */
public class CustomDatePicker extends JXDatePicker {

    public CustomDatePicker() {
        super();
        getEditor().setBackground(Color.WHITE);
    }

    public void setDate(LocalDateTime date) {
        setDate(date, true);
    }

    public void setDate(LocalDateTime date, boolean commit) {
        super.setDate(Date.from(date.atZone(ZoneId.systemDefault()).toInstant()));

        if(commit) {
            try {
                super.commitEdit();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    public LocalDateTime getLocalDateTime() {
        return LocalDateTime.ofInstant(super.getDate().toInstant(), ZoneId.systemDefault());
    }
}
