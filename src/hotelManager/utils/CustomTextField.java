package hotelManager.utils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

/**
 * JTextField with custom background color
 */
public class CustomTextField extends JTextField {

    private static final Color PLACEHOLDER_COLOR = Color.GRAY;
    private static final Color DEFAULT_COLOR = Color.BLACK;
    private String placeholder = null;
    private boolean placeholderDisplay = true;

    public CustomTextField() {
        super();
        setBackground(Color.WHITE);
    }

    public CustomTextField(int i) {
        super(i);
        setBackground(Color.WHITE);
    }

    public void setPlaceholder(String placeholder) {
        if (this.placeholder == null) {
            enablePlaceholder();
        }

        this.placeholder = placeholder;

        if (getText().isEmpty())
            showPlaceholder();
    }

    private void showPlaceholder() {
        setText(placeholder);
        setForeground(PLACEHOLDER_COLOR);
        placeholderDisplay = true;
    }

    private void hidePlaceholder() {
        setText("");
        setForeground(DEFAULT_COLOR);
        placeholderDisplay = false;
    }

    private void enablePlaceholder() {
        addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent focusEvent) {
                if (placeholder == null)
                    return;

                if (getText().equals(placeholder) && placeholderDisplay)
                    hidePlaceholder();
            }

            @Override
            public void focusLost(FocusEvent focusEvent) {
                if (getText().isEmpty())
                    showPlaceholder();
            }
        });
    }
}
