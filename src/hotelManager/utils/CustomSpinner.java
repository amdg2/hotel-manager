package hotelManager.utils;

import javax.swing.*;
import java.awt.*;

/**
 * JSpinner with custom background color
 */
public class CustomSpinner extends JSpinner {

    public CustomSpinner(SpinnerModel spinnerModel) {
        super(spinnerModel);
        getEditor().getComponent(0).setBackground(Color.WHITE);
    }
}
