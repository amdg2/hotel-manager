package hotelManager;

import java.awt.*;
import java.util.Properties;

/**
 * Application configuration constants
 */
public class AppConfig {

    private static Properties properties = PropertyLoader.getProperties();
    /* <UI Parameters> */
    public static String getHotelChainName(){return properties.getProperty("hotel.chain.name");}
    public static String getHotelChainLogo(){return properties.getProperty("hotel.chain.logo");}
    public static String getHotelPhotoPath(){return properties.getProperty("hotel.photo.path");}

    public static final Color BTN_BACKGROUND_COLOR = Color.WHITE;
    /* </UI Parameters> */

    /* </Application parameters> */

    public static final String APPLICATION_PROPERTIES = "resources/application.properties";
    public static final String APPLICATION_PROPERTIES_HEADER =
            "###################################################\n" +
            "#                                                 #\n" +
            "#         Application properties file             #\n" +
            "#                                                 #\n" +
            "###################################################\n" +
            "\n\n\n\n";
    public static final String APPLICATION_PROPERTIES_TITLE_UI = "#UI Parameters \n";
    public static final String APPLICATION_PROPERTIES_TITLE_DB = "\n\n\n#Database Parameters \n";
    public static final String APPLICATION_PROPERTIES_TITLE_ICON = "\n\n\n#Icons Parameters \n";
    public static final String APPLICATION_PROPERTIES_TITLE_OTHER = "\n\n\n#Other Parameters \n";

}
