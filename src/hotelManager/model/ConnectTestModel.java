package hotelManager.model;


import java.util.ArrayList;

/**
 * Class to test all of models
 */
public class ConnectTestModel {

    /**
     * Methods to nominal testing
     * @param args arg for the main
     */
    public static void main(String[] args){

        ArrayList<Hotel> listHotel;
        ArrayList<Employee> listEmployee;
        ArrayList<Employee> listManager;
        ArrayList<Room> listRoom;
        ArrayList<Function> listFunction;
        ArrayList<Customer> listCustomer;
        ArrayList<Reservation> listReservation;
        ArrayList<City> cityByZipCode;

        Hotel hotelTest;
        Function functionTest;
        City cityTest;

        /**
         * Object for Insertion test
         */
        Employee employeeInsertTest;
        Hotel hotelInsertTest;
        Customer customerInsertTest;
        Function functionInsertTest;
        Room roomInsertTest;

        /**
         * Object for Insertion test
         */
        Employee employeeUpdateTest;
        Hotel hotelUpdateTest;
        Customer customerUpdateTest;
        Function functionUpdateTest;
        Room roomUpdateTest;
        Reservation reserveUpdateTest;

        /**
         * Object for Delete Test
         */
        Employee employeeDelete = new Employee();
        Hotel hotelDelete =new Hotel();
        Customer customerDelete = new Customer();
        Function functionDelete = new Function();
        Room roomDelete = new Room();
        Reservation reserveDelete = new Reservation();


        int isUpdateEmployee;
        int isUpdateHotel;
        int isUpdateCustomer;
        int isUpdateFunction;
        int isUpdateReservation;
        int isUpdateRoom;

        int isDeleteEmployee;
        int isDeleteHotel;
        int isDeleteCustomer;
        int isDeleteFunction;
        int isDeleteReservation;
        int isDeleteRoom;

        int isCreateEmployee;
        int isCreateHotel;
        int isCreateCustomer;
        int isCreateFunction;
        int isCreateReservation;
        int isCreateRoom;

        //try {
            /*
            functionTest = DbManager.getInstance().getFunctionById(2);
            hotelTest = DbManager.getInstance().getHotelById(1);
            cityTest = DbManager.getInstance().getCityById(14381);
            cityByZipCode = DbManager.getInstance().getCityByZipCode(49510);
            */

            /**
             * List Test
             */
        /*
            listHotel = DbManager.getInstance().getListHotel();
            listRoom = DbManager.getInstance().getListRoom(hotelTest);
            listEmployee = DbManager.getInstance().getListEmployee(hotelTest);
            listManager = DbManager.getInstance().getListManagerHotel(hotelTest);
            listFunction = DbManager.getInstance().getListFunction();
            listCustomer = DbManager.getInstance().getListCustomer();
            listReservation = DbManager.getInstance().getListReservation(hotelTest);
          //*/

            /**
             * Object to insert
             */
            //employeeInsertTest = new Employee(0,"Yuri Testing de la mort qui tue","CP 628, 9500 Elit Av.",true,functionTest,hotelTest,cityTest);
            //hotelInsertTest = new Hotel(0,"Hotel test insertion","ESEO adresse",5,cityTest);
            //customerInsertTest = new Customer(0,new Date(),"A1A2A3B4BB","Kevin NGUYEN","Adresse ESEO",cityTest);
            //functionInsertTest = new Function(0,"Ingénieur");
            //roomInsertTest = new Room(0,800,1,1,500,hotelTest);

            /**
             * Object to delete instanciate id
             */
            //employeeDelete.setId(399);
            //hotelDelete.setId(102);
            //customerDelete.setId(402);
            //functionDelete.setId(11);
            //roomDelete.setId(3301);
            //reserveDelete.setId(2770);

            /**
             * Object to update
             */
            //employeeUpdateTest = new Employee(399,"Yuri Testing de la mort qui tue","CP 628, 9500 Elit Av.",true,functionTest,hotelTest,cityTest);
            //hotelUpdateTest = new Hotel(101,"Hotel test update","ESEO adresse",5,cityTest);
            //customerUpdateTest = new Customer(401,new Date(),"77777aazzz","Kevin NGUYEN","Adresse ESEO",cityTest);
            //functionUpdateTest = new Function(10,"Ingénieur");
            //roomUpdateTest = new Room(3300,800,1,1,11100,hotelTest);
            //reserveUpdateTest = new Reservation(new Date(),new Date(),roomUpdateTest,customerUpdateTest,2769,"canceled");

            /**
             * Execution Update
             */
            //isUpdateEmployee = DbManager.getInstance().updateEmployee(employeeUpdateTest);
            //isUpdateHotel = DbManager.getInstance().updateHotel(hotelUpdateTest);
            //isUpdateCustomer = DbManager.getInstance().updateCustomer(customerUpdateTest);
            //isUpdateFunction = DbManager.getInstance().updateFunction(functionUpdateTest);
            //isUpdateReservation = DbManager.getInstance().updateReservation(reserveUpdateTest);
            //isUpdateRoom = DbManager.getInstance().updateRoom(roomUpdateTest);

            /**
             * Execution Delete
             */
            //isDeleteEmployee = DbManager.getInstance().deleteEmployee(employeeDelete);
            //isDeleteHotel = DbManager.getInstance().deleteHotel(hotelDelete);
            //isDeleteCustomer = DbManager.getInstance().deleteCustomer(customerDelete);
            //isDeleteFunction = DbManager.getInstance().deleteFunction(functionDelete);
            //isDeleteRoom = DbManager.getInstance().deleteRoom(roomDelete);
            //isDeleteReservation = DbManager.getInstance().deleteReservation(reserveDelete);


            /**
             * Execution Insert
             */
            //isCreateEmployee = DbManager.getInstance().addEmployee(employeeInsertTest);
            //isCreateHotel = DbManager.getInstance().addHotel(hotelInsertTest);
            //isCreateCustomer = DbManager.getInstance().addCustomer(customerInsertTest);
            //isCreateFunction = DbManager.getInstance().addFunction(functionInsertTest);
            //isCreateRoom = DbManager.getInstance().addRoom(roomInsertTest);
            //isCreateReservation = DbManager.getInstance().addReservation(customerInsertTest,roomInsertTest,new Date(),new Date());

            /**
             * Update Test
             */
            //System.out.println(isUpdateEmployee);
            //System.out.println(isUpdateHotel);
            //System.out.println(isUpdateCustomer);
            //System.out.println(isUpdateFunction);
            //System.out.println(isUpdateReservation);
            //System.out.println(isUpdateRoom);

            /**
             * Delete Test
             */
            //System.out.println(isDeleteEmployee);
            //System.out.println(isDeleteHotel);
            //System.out.println(isDeleteCustomer);
            //System.out.println(isDeleteFunction);
            //System.out.println(isDeleteRoom);
            //System.out.println(isDeleteReservation);


            /**
             * Create Test
             */
            //System.out.println(isCreateEmployee);
            //System.out.println(isCreateHotel);
            //System.out.println(isCreateCustomer);
            //System.out.println(isCreateFunction);
            //System.out.println(isCreateRoom);
            //System.out.println(isCreateReservation);

            /**
             * Loop for Test List
             */
            //cityByZipCode.stream().forEach(System.out::println);

            /*
            for (Reservation reservation: listReservation){
                System.out.println(reservation.getId() + " : " + reservation.getCustomer().getName());
            }
            //*/

            /*
            for (Customer customer: listCustomer){
                System.out.println(customer.getId() + " : " + customer.getName());
            }
            //*/
            /*
            for (Function function : listFunction){
                System.out.println(function.getId() + " : " + function.getName());
            }
            //*/

            /*
            for (Room room : listRoom){
                System.out.println(room.getNumber() + " : " + room.getHotel().getName());
            }
            //*/
            /*
            for (Employee employee : listEmployee){
                System.out.println(employee.getName() + " : " + employee.getFunction().getName());
            }
            //*/
            /*
            listManager.stream().forEach(employee -> System.out.println(employee.getName() + " : " + employee.getFunction().getName()));
            //*/
            /*
            for (Hotel hotel : listHotel){
                System.out.println(hotel.getName());
                System.out.println(hotel.getCity().getName());
            }
            //*/
       /* } catch (Exception e) {
            e.printStackTrace();
        }
        */
    }
}
