package hotelManager.model;

import hotelManager.controllers.AppController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Hotel model
 */
public class Hotel {

    private int id;
    private String name;
    private String address;
    private int category;
    private City city;

    public Hotel() {

    }

    public Hotel(int id, String name, String address, int category, City city) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.category = category;
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public List<Employee> getManager() {
        try {
            return DbManager.getInstance().getListManagerHotel(this);
        } catch (Exception e) {
            AppController.getInstance().showError(e);
        }

        return new ArrayList<>();
    }

    public List<Room> getRooms() {
        try {
            return DbManager.getInstance().getListRoom(this);
        } catch (Exception e) {
            AppController.getInstance().showError(e);
            return new ArrayList<>();
        }
    }

    public List<Reservation> getReservations(){
        try {
            return DbManager.getInstance().getListReservation(this);
        } catch (Exception e) {
            AppController.getInstance().showError(e);
            return new ArrayList<>();
        }
    }

    public List<Room> getTwinRooms() {
        return getRooms().stream().filter(room -> room.getNbDoubleBed() > 0).collect(Collectors.toList());
    }

    public List<Room> getSingleRooms() {
        return getRooms().stream().filter(room -> room.getNbDoubleBed() == 0).collect(Collectors.toList());
    }

    @Override
    public String toString() {
        return name;
    }

    public List<Employee> getEmployees() {
        try {
            return DbManager.getInstance().getListEmployee(this);
        } catch (Exception e) {
            AppController.getInstance().showError(e);
            return new ArrayList<>();
        }
    }

    public List<Customer> getCustomers() {
        try {
            return DbManager.getInstance().getListCustomerByHotel(this);
        } catch (Exception e) {
            AppController.getInstance().showError(e);
            return new ArrayList<>();
        }
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Hotel hotel = (Hotel) o;

        return id == hotel.id &&
                category == hotel.category &&
                name.equals(hotel.name) &&
                address.equals(hotel.address) &&
                (city != null ? city.equals(hotel.city) : hotel.city == null);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + name.hashCode();
        result = 31 * result + address.hashCode();
        result = 31 * result + category;
        result = 31 * result + (city != null ? city.hashCode() : 0);
        return result;
    }
}
