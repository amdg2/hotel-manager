package hotelManager.model;

import hotelManager.controllers.AppController;

/**
 * Employee model
 */
public class Employee {

    public static final Employee EMPTY_EMPLOYEE = new Employee(-1, "Aucun", "", false, null, null, null);
    private int id;
    private String name;
    private String address;
    private boolean isResponsible;
    private Function function;
    private Hotel hotel;
    private City city;
    private boolean newRecord = false;

    public Employee() {

    }

    public Employee(int id, String name, String address, boolean isResponsible, Function function, Hotel hotel, City city) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.isResponsible = isResponsible;
        this.function = function;
        this.hotel = hotel;
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isResponsible() {
        return isResponsible;
    }

    public void setResponsible(boolean responsible) {
        isResponsible = responsible;
    }

    public Function getFunction() {
        return function;
    }

    public void setFunction(Function function) {
        this.function = function;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return name;
    }

    public boolean save() {
        try {
            int updateStatus;
            if (newRecord) {
                updateStatus = DbManager.getInstance().addEmployee(this);
                newRecord = false;
            } else updateStatus = DbManager.getInstance().updateEmployee(this);

            return updateStatus == 1;
        } catch (Exception ex) {
            AppController.getInstance().showError(ex);
            return false;
        }
    }

    public void delete() {
        try {
            DbManager.getInstance().deleteEmployee(this);
            newRecord = true;
        } catch (Exception e) {
            AppController.getInstance().showError(e);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Employee employee = (Employee) o;

        return id == employee.id &&
                isResponsible == employee.isResponsible &&
                name.equals(employee.name) &&
                address.equals(employee.address) &&
                function.equals(employee.function) &&
                hotel.equals(employee.hotel) &&
                (city != null ? city.equals(employee.city) : employee.city == null);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + name.hashCode();
        result = 31 * result + address.hashCode();
        result = 31 * result + (isResponsible ? 1 : 0);
        result = 31 * result + function.hashCode();
        result = 31 * result + hotel.hashCode();
        result = 31 * result + (city != null ? city.hashCode() : 0);
        return result;
    }

    public void setNewRecord(boolean newRecord) {
        this.newRecord = newRecord;
    }
}
