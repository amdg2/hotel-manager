package hotelManager.model;

import java.util.Date;

/**
 * Statistics model
 */
public class Stat {

    private int id;
    private Date startDate;
    private Date endDate;
    private float turnover;
    private int nbReservation;
    private String modeStat; // three values possible 'M' for a stat on a month, 'W' for week , 'Y' for a year
    private Hotel hotel;

    public Stat() {
    }

    public Stat(int id, Date startDate, Date endDate, float turnover, int nbReservation,String modeStat, Hotel hotel) {
        this.id = id;
        this.startDate = startDate;
        this.endDate = endDate;
        this.turnover = turnover;
        this.nbReservation = nbReservation;
        this.modeStat = modeStat;
        this.hotel = hotel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public float getTurnover() {
        return turnover;
    }

    public void setTurnover(float turnover) {
        this.turnover = turnover;
    }

    public int getNbReservation() {
        return nbReservation;
    }

    public void setNbReservation(int nbReservation) {
        this.nbReservation = nbReservation;
    }

    public String getModeStat() {
        return modeStat;
    }

    public void setModeStat(String modeStat) {
        this.modeStat = modeStat;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }
}
