package hotelManager.model;

import hotelManager.controllers.AppController;

/**
 * Room model
 */
public class Room {

    private int id;
    private int number;
    private int nbSimpleBed;
    private int nbDoubleBed;
    private float price;
    private Hotel hotel;
    private String statut;
    private boolean newRecord = false;

    public Room() {

    }

    public Room(int id, int number, int nbSimpleBed, int nbDoubleBed, float price, Hotel hotel) {
        this.id = id;
        this.number = number;
        this.nbSimpleBed = nbSimpleBed;
        this.nbDoubleBed = nbDoubleBed;
        this.price = price;
        this.hotel = hotel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getNbSimpleBed() {
        return nbSimpleBed;
    }

    public void setNbSimpleBed(int nbSimpleBed) {
        this.nbSimpleBed = nbSimpleBed;
    }

    public int getNbDoubleBed() {
        return nbDoubleBed;
    }

    public void setNbDoubleBed(int nbDoubleBed) {
        this.nbDoubleBed = nbDoubleBed;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    @Override
    public String toString() {
        return Integer.toString(number);
    }

    public boolean save() {
        try {
            int updateStatus;
            if (newRecord) {
                updateStatus = DbManager.getInstance().addRoom(this);
                newRecord = false;
            } else updateStatus = DbManager.getInstance().updateRoom(this);

            return updateStatus == 1;
        } catch (Exception ex) {
            AppController.getInstance().showError(ex);
            return false;
        }
    }

    public void delete() {
        try {
            DbManager.getInstance().deleteRoom(this);
            newRecord = true;
        } catch (Exception e) {
            AppController.getInstance().showError(e);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Room room = (Room) o;

        return id == room.id &&
                number == room.number &&
                nbSimpleBed == room.nbSimpleBed &&
                nbDoubleBed == room.nbDoubleBed &&
                Float.compare(room.price, price) == 0 &&
                hotel.equals(room.hotel) &&
                (statut != null ? statut.equals(room.statut) : room.statut == null);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + number;
        result = 31 * result + nbSimpleBed;
        result = 31 * result + nbDoubleBed;
        result = 31 * result + (price != +0.0f ? Float.floatToIntBits(price) : 0);
        result = 31 * result + hotel.hashCode();
        result = 31 * result + (statut != null ? statut.hashCode() : 0);
        return result;
    }

    public void setNewRecord(boolean newRecord) {
        this.newRecord = newRecord;
    }
}
