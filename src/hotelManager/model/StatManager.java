package hotelManager.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Manage the statistics in the database
 */
public class StatManager {

    private StatManager() {

    }

    public static float calculateTurnOverOnAPeriod(ArrayList<Reservation> listReservation){
        float turnover = 0;

        for (Reservation reservation:listReservation) {
            turnover +=  reservation.getRoom().getPrice();
        }

        return turnover;
    }

    public static int calculateNbReservationOnAPeriod(ArrayList<Reservation> listReservation){

        return listReservation.size();
    }

    public static boolean haveStatOnThisPeriod(Date startDate,Hotel hotel,String modeStat){
        boolean haveStat = false;
        try {

            Stat stat = DbManager.getInstance().getStat(startDate,hotel,modeStat);
            haveStat = (stat != null);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return haveStat;
    }

    public ArrayList<Stat> displayStat(Date startDate, Hotel hotel, String modeStat){
        ArrayList<Stat> listStat = new ArrayList<>();
        Stat stat;

        try {
            DbManager connection = DbManager.getInstance();
            if (haveStatOnThisPeriod(startDate,hotel,modeStat)){
                listStat = connection.getStatForAPeriod(startDate,hotel,modeStat);
            }else{
                switch (modeStat) {
                    case "W":
                        stat = createStat(startDate, hotel, modeStat);
                        listStat.add(stat);
                        break;
                    case "M":
                        for (int i = 0; i < 4; i++) {
                            stat = createStat(startDate, hotel, modeStat);
                            listStat.add(stat);
                        }

                        break;
                    case "Y":
                        for (int i = 0; i < 12; i++) {
                            stat = createStat(startDate, hotel, modeStat);
                            listStat.add(stat);
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return listStat;
    }
/*
    public static double getDaysBetweenDates(Date startDate, Date endDate) {
        double result = Double.POSITIVE_INFINITY;
        if (startDate != null && endDate != null) {
            final long MILLISECONDS_PER_DAY = 1000 * 60 * 60 * 24;
            Calendar aCal = Calendar.getInstance();
            aCal.setTime(startDate);
            long aFromOffset = aCal.get(Calendar.DST_OFFSET);
            aCal.setTime(endDate);
            long aToOffset = aCal.get(Calendar.DST_OFFSET);
            long aDayDiffInMili = (endDate.getTime() + aToOffset) - (startDate.getTime() + aFromOffset);
            result = ((double) aDayDiffInMili / MILLISECONDS_PER_DAY);
        }
        return result;
    }
*/
    public static Stat createStat(Date startDate , Hotel hotel, String modeStat ){
        Date endDate = getTheRightEndDate(startDate,modeStat);
        ArrayList<Reservation> listReservation;
        Stat stat = new Stat();
        try {
            listReservation = DbManager.getInstance().getListOfReservationForPeriod(startDate,endDate);
            stat.setTurnover(calculateTurnOverOnAPeriod(listReservation));
            stat.setNbReservation(calculateNbReservationOnAPeriod(listReservation));
            stat.setStartDate(startDate);
            stat.setEndDate(endDate);
            stat.setHotel(hotel);
            stat.setModeStat(modeStat);
            DbManager.getInstance().addStat(stat);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stat;

    }

    public static Date getTheRightEndDate(Date startDate , String mode){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        switch (mode){
            case "W" :
                calendar.add(Calendar.DAY_OF_MONTH,7);
                break;
            case "M" :
                calendar.add(Calendar.MONTH,1);
                break;
            case "Y" :
                break;
            default:
                calendar.add(Calendar.YEAR,1);
                break;
        }
        return new Date(calendar.getTimeInMillis());

    }

}
