package hotelManager.model;

import hotelManager.controllers.AppController;

import java.util.Date;

/**
 * Customer model
 */
public class Customer {


    private int id;
    private Date dateOfBirth;
    private String customerNumber;
    private String name;
    private String address;
    private City city;
    private boolean newRecord = false;

    public Customer() {

    }

    public Customer(int id, Date dateOfBirth, String customerNumber, String name, String address, City city) {
        this.id = id;
        this.dateOfBirth = dateOfBirth;
        this.customerNumber = customerNumber;
        this.name = name;
        this.address = address;
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getCustomerNumber() {
        return customerNumber;
    }

    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public boolean save() {
        try {
            int updateStatus;
            if (newRecord) {
            updateStatus = DbManager.getInstance().addCustomer(this);
                newRecord = false;
            } else updateStatus = DbManager.getInstance().updateCustomer(this);

            return updateStatus == 1;
        } catch (Exception ex) {
            AppController.getInstance().showError(ex);
            return false;
        }
    }

    public void delete() {
        try {
            DbManager.getInstance().deleteCustomer(this);
            newRecord = true;
        } catch (Exception e) {
            AppController.getInstance().showError(e);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Customer customer = (Customer) o;

        return id == customer.id &&
                dateOfBirth.equals(customer.dateOfBirth) &&
                customerNumber.equals(customer.customerNumber) &&
                name.equals(customer.name) &&
                address.equals(customer.address) &&
                (city != null ? city.equals(customer.city) : customer.city == null);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + dateOfBirth.hashCode();
        result = 31 * result + customerNumber.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + address.hashCode();
        result = 31 * result + (city != null ? city.hashCode() : 0);
        return result;
    }

    public void setNewRecord(boolean newRecord) {
        this.newRecord = newRecord;
    }

    @Override
    public String toString() {
        return name + " - " +customerNumber;
    }
}
