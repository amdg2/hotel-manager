package hotelManager.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by Kevin on 29/01/2016.
 */
public class SqlConnection {

    private String username;
    private String password;
    private String typeDb;
    private String hostname;
    private String port;
    private String sid;
    private Connection connect;

    public SqlConnection(String username, String password, String typeDb, String hostname, String port, String sid) {
        this.username = username;
        this.password = password;
        this.typeDb = typeDb;
        this.hostname = hostname;
        this.port = port;
        this.sid = sid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTypeDb() {
        return typeDb;
    }

    public void setTypeDb(String typeDb) {
        this.typeDb = typeDb;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public Connection getConnection() throws SQLException, ClassNotFoundException {
        if (connect == null){
            System.out.println("-------- Oracle JDBC Connection Testing ------");
            Class.forName("oracle.jdbc.driver.OracleDriver");
            System.out.println("Oracle JDBC Driver Registered!");
            Properties connectionProps = new Properties();
            connectionProps.put("user", this.getUsername());
            connectionProps.put("password", this.getPassword());
            connect = DriverManager.getConnection(
                    "jdbc:"+this.getTypeDb() + ":thin:@" + this.hostname +":" + this.getPort()+":"+ this.getSid(), connectionProps);
            if (connect != null) {
                System.out.println("You made it, take control your database now!");
            } else {
                System.out.println("Failed to make connection!");
            }
        }

        return connect;
    }
}
