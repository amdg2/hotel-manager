package hotelManager.model;

import hotelManager.PropertyLoader;

import java.sql.*;
import java.sql.Date;
import java.util.*;

/**
 * DbManager
 */
@SuppressWarnings("SqlResolve")
public class DbManager {

    private static  DbManager instance;

    private Connection connection;
    private SqlConnection connectionProps;

    /**
     *
     * private constructor to not instanciate a DbManager object
     * @throws Exception
     */
    private DbManager()throws Exception {
        Properties properties = PropertyLoader.getProperties();
        String username = properties.getProperty("jdbc.username");
        String password = properties.getProperty("jdbc.password");
        String typeDb = properties.getProperty("jdbc.dbType");
        String hostname = properties.getProperty("jdbc.hostname");
        String port = properties.getProperty("jdbc.port");
        String sid = properties.getProperty("jdbc.sid");


        connectionProps = new SqlConnection(username,password,typeDb,hostname,port,sid);
        connection = connectionProps.getConnection();

    }

    /**
     * have an instance of DbManager
     * @return an instance
     * @throws Exception if error to get the instance
     */
    public  static DbManager getInstance()throws Exception {
        if(instance == null) {
            instance = new DbManager();
        }
        return instance;
    }

    /**
     * Delete an instance of DbManager
     */
    public static void deleteInstance() {
        try {
            if (instance != null) {
                instance.closeSqlConnection();
                instance = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Close the sql Connection
     * @throws SQLException if an error occured in the close
     */
    public void closeSqlConnection() throws SQLException {
        connection.close();
    }

    /**
     * All GetList
     */



    /**
     * Get the list of employees of an hotel
     *
     * @param hotel the hotel
     * @return an ArrayList of employees
     * @throws SQLException
     */
    public ArrayList<Employee> getListEmployee(Hotel hotel) throws SQLException {
        ArrayList<Employee> listEmployee;
        String query = "SELECT * FROM employees WHERE id_hotel= ?";
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setInt(1, hotel.getId());
        ResultSet result = ps.executeQuery();
        listEmployee = convertResultToListEmployee(result);

        ps.close();
        return listEmployee;
    }

    /**
     * Get the list of customers of an hotel
     *
     * @param hotel the hotel
     * @return an ArrayList of customers
     * @throws SQLException
     */
    public ArrayList<Customer> getListCustomerByHotel(Hotel hotel) throws SQLException {
        ArrayList<Customer> listCustomer;
        String query = "SELECT * FROM client,reserve WHERE reserve.id_client = client.id_client AND " +
                "reserve.id_chambre IN (SELECT id_chambre FROM CHAMBRE WHERE id_hotel = ?)";
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setInt(1, hotel.getId());
        ResultSet result = ps.executeQuery();
        listCustomer = convertResultToListCustomer(result);

        ps.close();
        return listCustomer;
    }

    /**
     * Return the list of responsible people in an hotel
     *
     * @param hotel the hotel
     * @return an Array of employee
     * @throws SQLException
     */
    public ArrayList<Employee> getListManagerHotel(Hotel hotel) throws SQLException {
        ArrayList<Employee> listEmployee;
        String query = "SELECT * FROM employees WHERE id_hotel= ? AND estreponsable_employee = ? ";
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setInt(1, hotel.getId());
        ps.setInt(2, 1);
        ResultSet result = ps.executeQuery();
        listEmployee = convertResultToListEmployee(result);

        ps.close();
        return listEmployee;
    }

    /**
     * Get the list of all room in an hotel
     * @param hotel the hotel
     * @return an ArrayList of rooms
     * @throws SQLException
     */
    public ArrayList<Room> getListRoom(Hotel hotel) throws SQLException {
        ArrayList<Room> listRoom = new ArrayList<>();
        String query = "SELECT * FROM chambre WHERE id_hotel= ?";
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setInt(1, hotel.getId());
        ResultSet result = ps.executeQuery();

        while (result.next()) {
            Room room = new Room();
            room.setId(result.getInt("id_chambre"));
            room.setNumber(result.getInt("num_chambre"));
            room.setNbDoubleBed(result.getInt("nblitdouble_chambre"));
            room.setNbSimpleBed(result.getInt("nblitsimple_chambre"));
            room.setHotel(getHotelById(result.getInt(("id_hotel"))));
            room.setPrice(result.getFloat("prix_chambre"));
            room.setStatut(result.getString("statut_chambre"));
            listRoom.add(room);

        }
        ps.close();
        result.close();
        return listRoom;
    }

    /**
     * Get the list of all the function
     * @return an ArrayList of Function
     * @throws SQLException
     */
    public ArrayList<Function> getListFunction() throws SQLException {
        ArrayList<Function> listFunction = new ArrayList<>();
        String query = "SELECT * FROM fonction ";
        PreparedStatement ps = connection.prepareStatement(query);
        ResultSet result = ps.executeQuery();

        while (result.next()) {
            Function function = new Function();
            function.setId(result.getInt("id_fonction"));
            function.setName(result.getString("nom_fonction"));
            listFunction.add(function);
        }
        ps.close();
        result.close();
        return listFunction;
    }

    /**
     * Get the list of all reservations in an hotel
     * @param hotel the hotel
     * @return an ArrayList of reservation
     * @throws SQLException
     */
    public ArrayList<Reservation> getListReservation(Hotel hotel) throws SQLException {
        ArrayList<Reservation> listReservation;
        String query = "SELECT * FROM reserve WHERE id_chambre IN (SELECT id_chambre FROM chambre WHERE id_hotel = ?)";
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setInt(1, hotel.getId());

        ResultSet result = ps.executeQuery();
        listReservation = convertResultToListReservation(result);
        ps.close();
        result.close();
        return listReservation;
    }

    /**
     * Get the list of all reservations for a room
     * @param room the room
     * @return an ArrayList of reservation
     * @throws SQLException
     */
    public ArrayList<Reservation> getListReservationRoom(Room room) throws SQLException {
        ArrayList<Reservation> listReservation;
        String query = "SELECT * FROM reserve WHERE id_chambre = ?";
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setInt(1, room.getId());

        ResultSet result = ps.executeQuery();
        listReservation = convertResultToListReservation(result);
        ps.close();
        result.close();
        return listReservation;
    }

    /**
     * Get the list of all the customer
     * @return An ArrayList of customers
     * @throws SQLException
     */
    public ArrayList<Customer> getListCustomer() throws SQLException {
        ArrayList<Customer> listCustomer;
        String query = "SELECT * FROM client";
        PreparedStatement ps = connection.prepareStatement(query);
        ResultSet result = ps.executeQuery();
        listCustomer = convertResultToListCustomer(result);
        ps.close();
        result.close();
        return listCustomer;
    }

    /**
     * Get the list of Hotels
     * @return list of hotel
     * @throws SQLException
     */
    public ArrayList<Hotel> getListHotel() throws SQLException {
        ArrayList<Hotel> listHotel = new ArrayList<>();
        String query = "SELECT * FROM hotel";
        PreparedStatement ps = connection.prepareStatement(query);
        ResultSet result = ps.executeQuery();

        while (result.next()) {
            Hotel hotel = new Hotel();
            hotel.setId(result.getInt("id_hotel"));
            hotel.setName(result.getString("nom_hotel"));
            hotel.setAddress(result.getString("adresse_hotel"));
            hotel.setCategory(result.getInt("categorie_hotel"));
            hotel.setCity(getCityById(result.getInt("id_ville")));
            listHotel.add(hotel);

        }
        ps.close();
        result.close();
        return listHotel;
    }


    /**
     * All GetById
     */

    /**
     * Get the list of all cities
     *
     * @return An ArrayList of all the cities
     * @throws SQLException
     */
    public ArrayList<City> getListCity() throws SQLException {
        ArrayList<City> listCity = new ArrayList<>();
        String query = "SELECT * FROM Ville ";
        PreparedStatement ps = connection.prepareStatement(query);
        ResultSet result = ps.executeQuery();
        while (result.next()) {
            City city = new City();
            city.setId(result.getInt("id_ville"));
            city.setName(result.getString("nom_ville"));
            city.setZipCode(result.getString("codepostal_ville"));
            listCity.add(city);
        }
        ps.close();
        result.close();
        return listCity;
    }

    /**
     *
     * Get the city by his id
     * @param id    the id of the city
     * @return a city
     * @throws SQLException
     */
    public  City getCityById(int id) throws SQLException {
        City city = new City();
        String query = "SELECT * FROM Ville WHERE id_ville = ?";
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setInt(1, id);
        ResultSet result = ps.executeQuery();
        while (result.next()) {
            city.setId(result.getInt("id_ville"));
            city.setName(result.getString("nom_ville"));
            city.setZipCode(result.getString("codePostal_ville"));
        }
        ps.close();
        result.close();
        return city;
    }

    /**
     * Get a room by id
     * @param id    the id of the room
     * @return a room
     * @throws SQLException
     */
    public Room getRoomById(int id) throws SQLException {
        Room room = new Room();
        String query = "SELECT * FROM chambre WHERE id_chambre = ?";
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setInt(1, id);
        ResultSet result = ps.executeQuery();
        while (result.next()) {
            room.setId(result.getInt("id_chambre"));
            room.setNumber(result.getInt("num_chambre"));
            room.setNbDoubleBed(result.getInt("nblitdouble_chambre"));
            room.setNbSimpleBed(result.getInt("nblitsimple_chambre"));
            room.setHotel(getHotelById(result.getInt(("id_hotel"))));
            room.setPrice(result.getFloat("prix_chambre"));
            room.setStatut(result.getString("statut_chambre"));
        }
        ps.close();
        result.close();
        return room;
    }

    /**
     * get a customer by id
     * @param id    the id of the customer
     * @return a customer
     * @throws SQLException
     */
    public Customer getCustomerById(int id) throws SQLException {
        Customer customer = new Customer();
        String query = "SELECT * FROM client WHERE id_client = ?";
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setInt(1, id);

        ResultSet result = ps.executeQuery();
        while (result.next()) {
            customer.setId(result.getInt("id_client"));
            customer.setDateOfBirth(result.getDate("datenaissance_client"));
            customer.setCustomerNumber(result.getString("num_client"));
            customer.setName(result.getString("nom_client"));
            customer.setAddress(result.getString("adresse_client"));
            customer.setCity(getCityById(result.getInt("id_ville")));
        }
        ps.close();
        result.close();
        return customer;
    }

    /**
     *
     * Get the hotel by his id
     * @param id    the id of the hotel
     * @return a hotel
     * @throws SQLException
     */
    public  Hotel getHotelById(int id) throws SQLException {
        Hotel hotel = new Hotel();
        String query = "SELECT * FROM Hotel WHERE id_hotel = ?";
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setInt(1, id);
        ResultSet result = ps.executeQuery();
        while (result.next()) {
            hotel.setId(result.getInt("id_hotel"));
            hotel.setName(result.getString("nom_hotel"));
            hotel.setAddress(result.getString("adresse_hotel"));
            hotel.setCategory(result.getInt("categorie_hotel"));
            hotel.setCity(getCityById(result.getInt("id_ville")));
        }
        ps.close();
        result.close();
        return hotel;
    }

    /**
     *
     * Get the Function by his id
     * @param id the id of the function
     * @return a function
     * @throws SQLException
     */
    public Function getFunctionById(int id) throws SQLException {
        Function function = new Function();
        String query = "SELECT * FROM Fonction WHERE id_fonction = ?";
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setInt(1, id);

        ResultSet result = ps.executeQuery();
        while (result.next()) {
            function.setId(result.getInt("id_fonction"));
            function.setName(result.getString("nom_fonction"));

        }
        ps.close();
        result.close();
        return function;
    }

    /**
     * Get a list of city by zipcode
     * @param zipcode a zipcode
     * @return a list of city
     * @throws SQLException
     */
    public ArrayList<City> getCityByZipCode(String zipcode) throws SQLException {
        ArrayList<City> listCity = new ArrayList<>();
        String query = "SELECT * FROM Ville WHERE codePostal_ville = ?";
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setString(1, zipcode);
        ResultSet result = ps.executeQuery();

        while (result.next()) {
            City city = new City();
            city.setId(result.getInt("id_ville"));
            city.setName(result.getString("nom_ville"));
            city.setZipCode(result.getString("codePostal_ville"));
            listCity.add(city);

        }
        return listCity;
    }

    /**
     * override of getCityByZipCode to have a zipcode in int
     * @param zipcode a zipcode
     * @return a list of city
     * @throws SQLException
     */
    public ArrayList<City> getCityByZipCode(int zipcode) throws SQLException {
        String zipcodeStr = String.format("%d", zipcode);

        return getCityByZipCode(zipcodeStr);
    }


    /**
     * Update
     */

    /**
     * update an employee
     * @param employee the employee have to update
     * @return the status of the update
     * @throws SQLException
     */
    public int updateEmployee(Employee employee) throws SQLException {
        String updateQuery = " UPDATE employees SET nom_employee = ?,adresse_employee = ?,estreponsable_employee = ?,id_hotel= ?,id_ville = ?,id_fonction = ? WHERE id_employee = ?";
        PreparedStatement ps = connection.prepareStatement(updateQuery);
        ps.setString(1, employee.getName());
        ps.setString(2, employee.getAddress());
        ps.setInt(3, (employee.isResponsible() ? 1 : 0));
        ps.setInt(4, employee.getHotel().getId());
        ps.setInt(5, employee.getCity().getId());
        ps.setInt(6, employee.getFunction().getId());
        ps.setInt(7, employee.getId());

        int isUpdate = ps.executeUpdate();
        ps.close();

        return isUpdate;
    }

    /**
     * update a customer
     *
     * @param customer the customer have to update
     * @return the status of the update
     * @throws SQLException
     */
    public int updateCustomer(Customer customer) throws SQLException {
        String updateQuery = " UPDATE client SET datenaissance_client = ?,num_client = ?,nom_client = ?,adresse_client= ?,id_ville = ? WHERE id_client = ?";
        PreparedStatement ps = connection.prepareStatement(updateQuery);
        ps.setDate(1, new Date(customer.getDateOfBirth().getTime()));
        ps.setString(2, customer.getCustomerNumber());
        ps.setString(3, customer.getName());
        ps.setString(4, customer.getAddress());
        ps.setInt(5, customer.getCity().getId());
        ps.setInt(6, customer.getId());

        int isUpdate = ps.executeUpdate();
        ps.close();

        return isUpdate;
    }

    /**
     * update a function
     *
     * @param function the function have to update
     * @return the status of the update
     * @throws SQLException
     */
    public int updateFunction(Function function) throws SQLException {
        String updateQuery = " UPDATE fonction SET nom_fonction = ? WHERE id_fonction = ?";
        PreparedStatement ps = connection.prepareStatement(updateQuery);
        ps.setString(1, function.getName());
        ps.setInt(2, function.getId());

        int isUpdate = ps.executeUpdate();
        ps.close();

        return isUpdate;
    }

    /**
     * update an hotel
     *
     * @param hotel the hotel have to update
     * @return the status of the update
     * @throws SQLException
     */
    public int updateHotel(Hotel hotel) throws SQLException {
        String updateQuery = " UPDATE hotel SET nom_hotel = ?,adresse_hotel = ?,categorie_hotel = ?,id_ville = ? WHERE id_hotel = ?";
        PreparedStatement ps = connection.prepareStatement(updateQuery);
        ps.setString(1, hotel.getName());
        ps.setString(2, hotel.getAddress());
        ps.setInt(3, hotel.getCategory());
        ps.setInt(4, hotel.getCity().getId());
        ps.setInt(5, hotel.getId());

        int isUpdate = ps.executeUpdate();
        ps.close();

        return isUpdate;
    }

    /**
     * update a reservation
     *
     * @param reservation the reservation have to update
     * @return the status of the update
     * @throws SQLException
     */
    public int updateReservation(Reservation reservation) throws SQLException {
        String updateQuery = " UPDATE reserve SET datedebut_reservation = ?,datefin_reservation = ?,id_client = ?,id_chambre = ?,statut_reserve = ? WHERE id_reserve = ?";
        PreparedStatement ps = connection.prepareStatement(updateQuery);
        ps.setDate(1, new Date(reservation.getStartDate().getTime()));
        ps.setDate(2, new Date(reservation.getEndDate().getTime()));
        ps.setInt(3, reservation.getCustomer().getId());
        ps.setInt(4, reservation.getRoom().getId());
        ps.setString(5, reservation.getStatut());
        ps.setInt(6, reservation.getId());

        int isUpdate = ps.executeUpdate();
        ps.close();

        return isUpdate;
    }

    /**
     * update a room
     *
     * @param room the room have to update
     * @return the status of the update
     * @throws SQLException
     */
    public int updateRoom(Room room) throws SQLException {
        String updateQuery = " UPDATE chambre SET num_chambre = ?,nblitsimple_chambre = ?,nblitdouble_chambre = ?,prix_chambre = ?,id_hotel = ?,statut_chambre = ? WHERE id_chambre = ?";
        PreparedStatement ps = connection.prepareStatement(updateQuery);
        ps.setInt(1, room.getNumber());
        ps.setInt(2, room.getNbSimpleBed());
        ps.setInt(3, room.getNbDoubleBed());
        ps.setFloat(4, room.getPrice());
        ps.setInt(5, room.getHotel().getId());
        ps.setString(6, room.getStatut());
        ps.setInt(7, room.getId());

        int isUpdate = ps.executeUpdate();
        ps.close();

        return isUpdate;
    }

    /**
     * Delete
     */

    /**
     * delete an object
     * @param id    the id of the object
     * @param typeOfId the type of the object
     * @return 1 if success
     * @throws SQLException
     */
    public int deleteObject(int id, String typeOfId) throws SQLException {
        String idName = "id_" + typeOfId;
        String tableName = (!typeOfId.equals("employee")) ? typeOfId : typeOfId + "s";


        String deleteQuery = "DELETE FROM " + tableName + " WHERE " + idName + " = ? ";
        PreparedStatement ps = connection.prepareStatement(deleteQuery);
        ps.setInt(1, id);
        int isDelete = ps.executeUpdate();
        ps.close();

        return isDelete;

    }

    /**
     * Delete an employee
     *
     * @param employee Employee to delete
     * @return 1 if deleted
     * @throws SQLException
     */
    public int deleteEmployee(Employee employee) throws SQLException {

        return deleteObject(employee.getId(), "employee");

    }

    /**
     * Delete a Customer
     *
     * @param customer Customer to delete
     * @return 1 if deleted
     * @throws SQLException
     */
    public int deleteCustomer(Customer customer) throws SQLException {
        /**
         * Delete all the reservation associated
         */
        String deleteQuery = "DELETE FROM reserve WHERE id_client = ? ";
        PreparedStatement ps = connection.prepareStatement(deleteQuery);
        ps.setInt(1, customer.getId());
        ps.executeUpdate();

        return deleteObject(customer.getId(), "client");
    }

    /**
     * Delete a function
     *
     * @param function function to delete
     * @return 1 if deleted
     * @throws SQLException
     */
    public int deleteFunction(Function function) throws SQLException {

        return deleteObject(function.getId(), "fonction");
    }

    /**
     * Delete an hotel
     *
     * @param hotel Hotel to delete
     * @return 1 if deleted
     * @throws SQLException
     */
    public int deleteHotel(Hotel hotel) throws SQLException {

        return deleteObject(hotel.getId(), "hotel");
    }

    /**
     * Delete a reservation
     *
     * @param reservation Reservation to delete
     * @return 1 if deleted
     * @throws SQLException
     */
    public int deleteReservation(Reservation reservation) throws SQLException {

        return deleteObject(reservation.getId(), "reserve");
    }

    /**
     * Insert
     */

    /**
     * Delete a room of an hotel
     *
     * @param room Room to delete
     * @return 1 if deleted
     * @throws SQLException
     */
    public int deleteRoom(Room room) throws SQLException {
        /**
         * Delete all the reservation associated
         */
        String deleteQuery = "DELETE FROM reserve WHERE id_chambre = ? ";
        PreparedStatement ps = connection.prepareStatement(deleteQuery);
        ps.setInt(1, room.getId());
        ps.executeUpdate();
        ps.close();

        return deleteObject(room.getId(), "chambre");
    }

    /**
     * Methods to add an employee in the database
     * @param employee Employee to add
     * @return 1 if added
     * @throws SQLException
     */
    public int addEmployee(Employee employee) throws SQLException {

        int newId = (employee.getId() != 0) ? employee.getId() : getLastId("employee") + 1;
        employee.setId(newId);

        String addQuery = "INSERT INTO employees(id_employee,nom_employee,adresse_employee,estreponsable_employee,id_fonction,id_hotel,id_ville) VALUES (?,?,?,?,?,?,?)";
        PreparedStatement ps = connection.prepareStatement(addQuery);

        ps.setInt(1, newId);
        ps.setString(2, employee.getName());
        ps.setString(3, employee.getAddress());
        ps.setInt(4, (employee.isResponsible() ? 1 : 0));
        ps.setInt(5, employee.getFunction().getId());
        ps.setInt(6, employee.getHotel().getId());
        ps.setInt(7, employee.getCity().getId());

        int isAdd = ps.executeUpdate();
        ps.close();


        return isAdd;
    }

    /**
     * Methods to add a customer in the database
     *
     * @param customer Customer to add
     * @return 1 if added
     * @throws SQLException
     */
    public int addCustomer(Customer customer) throws SQLException {

        int newId = getLastId("client") + 1;
        String addQuery = " INSERT INTO client(id_client,datenaissance_client,num_client,nom_client,adresse_client,id_ville) VALUES (?,?,?,?,?,?)";
        PreparedStatement ps = connection.prepareStatement(addQuery);

        ps.setInt(1, newId);
        ps.setDate(2, new Date(customer.getDateOfBirth().getTime()));
        ps.setString(3, customer.getCustomerNumber());
        ps.setString(4, customer.getName());
        ps.setString(5, customer.getAddress());
        ps.setInt(6, customer.getCity().getId());

        int isAdd = ps.executeUpdate();
        ps.close();

        return isAdd;
    }

    /**
     * Methods to add a function in the database
     *
     * @param function Function to add
     * @return 1 if added
     * @throws SQLException
     */
    public int addFunction(Function function) throws SQLException {

        int newId = (function.getId() != 0) ? function.getId() : getLastId("fonction") + 1;
        String addQuery = " INSERT INTO fonction(id_fonction,nom_fonction) VALUES (?,?) ";
        PreparedStatement ps = connection.prepareStatement(addQuery);
        ps.setInt(1, newId);
        ps.setString(2, function.getName());

        int isAdd = ps.executeUpdate();
        ps.close();

        return isAdd;
    }

    /**
     * Methods to add an hotel in the database
     *
     * @param hotel Hotel to add
     * @return 1 if added
     * @throws SQLException
     */
    public int addHotel(Hotel hotel) throws SQLException {

        int newId = (hotel.getId() != 0) ? hotel.getId() : getLastId("hotel") + 1;
        String addQuery = " INSERT INTO hotel(id_hotel,nom_hotel,adresse_hotel,categorie_hotel,id_ville) VALUES (?,?,?,?,?) ";
        PreparedStatement ps = connection.prepareStatement(addQuery);

        ps.setInt(1, newId);
        ps.setString(2, hotel.getName());
        ps.setString(3, hotel.getAddress());
        ps.setInt(4, hotel.getCategory());
        ps.setInt(5, hotel.getCity().getId());

        int isAdd = ps.executeUpdate();
        ps.close();

        return isAdd;
    }

    /**
     * Methods to add a reservation in the database
     *
     * @param customer Customer who make the reservation
     * @param room Room to book
     * @param startDate Start date
     * @param endDate End date
     * @return 1 if added
     * @throws SQLException
     */
    public int addReservation(Customer customer, Room room, java.util.Date startDate, java.util.Date endDate) throws SQLException {

        int newId = getLastId("reserve") + 1;
        String addQuery = " INSERT INTO reserve(datedebut_reservation,datefin_reservation,id_client,id_chambre,id_reserve,statut_reserve) VALUES (?,?,?,?,?,?) ";
        PreparedStatement ps = connection.prepareStatement(addQuery);

        ps.setDate(1, new Date(startDate.getTime()));
        ps.setDate(2, new Date(endDate.getTime()));
        ps.setInt(3, customer.getId());
        ps.setInt(4, room.getId());
        ps.setInt(5, newId);
        ps.setString(6, "new");

        int isAdd = ps.executeUpdate();
        ps.close();

        return isAdd;
    }

    /**
     * Methods to add a room in an hotel in the database
     *
     * @param room Room to add
     * @return 1 if added
     * @throws SQLException
     */
    public int addRoom(Room room) throws SQLException {

        int newId = (room.getId() != 0) ? room.getId() : getLastId("chambre") + 1;
        String addQuery = " INSERT INTO chambre(id_chambre,num_chambre,nblitsimple_chambre,nblitdouble_chambre,prix_chambre,id_hotel,statut_chambre) VALUES (?,?,?,?,?,?,?) ";
        PreparedStatement ps = connection.prepareStatement(addQuery);

        ps.setInt(1, newId);
        ps.setInt(2, room.getNumber());
        ps.setInt(3, room.getNbSimpleBed());
        ps.setInt(4, room.getNbDoubleBed());
        ps.setFloat(5, room.getPrice());
        ps.setInt(6, room.getHotel().getId());
        ps.setString(7, (!room.getStatut().isEmpty()?room.getStatut():"ready"));

        int isAdd = ps.executeUpdate();
        ps.close();


        return isAdd;
    }

    /**
     * Return the last id of the table
     *
     * @param objectName Table to query
     * @return the last id
     * @throws SQLException
     */
    private int getLastId(String objectName) throws SQLException {
        int lastId = 0;
        String tableName;
        String columnName = "id_" + objectName;
        Statement statement = connection.createStatement();
        tableName = objectName.equals("employee") ? objectName + "s" :objectName;
        String lastIdQuery = "SELECT " + columnName + " FROM " + tableName + " WHERE ROWNUM <=1 ORDER BY " + columnName + " DESC ";
        ResultSet result = statement.executeQuery(lastIdQuery);

        while (result.next()) {
            lastId = result.getInt(columnName);
        }

        return lastId;
    }


    /**
     * Methods to know if a room has a reservation
     * @param room the room
     * @return true if it is reserved
     */
    public boolean isReserved(Room room) throws SQLException{

        String query = "SELECT * FROM reserve WHERE id_chambre= ? AND (statut_reserve = 'new' OR statut_reserve = 'confirmed' OR statut_reserve = 'paid') ";
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setInt(1, room.getId());
        ResultSet result = ps.executeQuery();
        boolean isReserved = result.next();

        ps.close();

        return isReserved;
    }

    /**
     * Methods to know if a customer has a reservation
     * @param customer the customer
     * @return true if it is reserved
     */
    public boolean hadReservation(Customer customer) throws SQLException{

        String query = "SELECT * FROM reserve WHERE id_client = ? AND (statut_reserve = 'new' OR statut_reserve = 'confirmed' OR statut_reserve = 'paid') ";
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setInt(1, customer.getId());
        ResultSet result = ps.executeQuery();
        boolean isReserved = result.next();

        ps.close();

        return isReserved;
    }


    /**
     * Convertion in arrayList of object
     */

    /**
     * Return the list of employees in an ArrayList
     *
     * @param resultEmployee the result of the query
     * @return an Array of employee
     * @throws SQLException
     */
    private ArrayList<Employee> convertResultToListEmployee(ResultSet resultEmployee) throws SQLException {
        ArrayList<Employee> listOfEmployee = new ArrayList<>();
        while (resultEmployee.next()) {
            Employee employee = new Employee();
            employee.setId(resultEmployee.getInt("id_employee"));
            employee.setName(resultEmployee.getString("nom_employee"));
            employee.setAddress(resultEmployee.getString("adresse_employee"));
            employee.setResponsible(resultEmployee.getBoolean("estreponsable_employee"));
            employee.setHotel(getHotelById(resultEmployee.getInt(("id_hotel"))));
            employee.setCity(getCityById(resultEmployee.getInt("id_ville")));
            employee.setFunction(getFunctionById(resultEmployee.getInt("id_fonction")));
            listOfEmployee.add(employee);
        }
        resultEmployee.close();
        return listOfEmployee;
    }

    /**
     * Return the list of employees in an ArrayList
     *
     * @param resultCustomer the result of the query
     * @return an Array of employee
     * @throws SQLException
     */
    private ArrayList<Customer> convertResultToListCustomer(ResultSet resultCustomer) throws SQLException {
        ArrayList<Customer> listCustomer = new ArrayList<>();

        while (resultCustomer.next()) {
            Customer customer = new Customer();
            customer.setId(resultCustomer.getInt("id_client"));
            customer.setDateOfBirth(resultCustomer.getDate("datenaissance_client"));
            customer.setCustomerNumber(resultCustomer.getString("num_client"));
            customer.setName(resultCustomer.getString("nom_client"));
            customer.setAddress(resultCustomer.getString("adresse_client"));
            customer.setCity(getCityById(resultCustomer.getInt("id_ville")));
            listCustomer.add(customer);
        }
        resultCustomer.close();
        return listCustomer;
    }

    /**
     * Return the list of reservation in an ArrayList
     *
     * @param resultReservation the result of the query
     * @return an Array of reservation
     * @throws SQLException
     */
    private ArrayList<Reservation> convertResultToListReservation(ResultSet resultReservation) throws SQLException {
        ArrayList<Reservation> listOfReservation = new ArrayList<>();
        while (resultReservation.next()) {
            Reservation reservation = new Reservation();
            reservation.setId(resultReservation.getInt("id_reserve"));
            reservation.setRoom(getRoomById(resultReservation.getInt("id_chambre")));
            reservation.setCustomer(getCustomerById(resultReservation.getInt("id_client")));
            reservation.setEndDate(resultReservation.getDate("datefin_reservation"));
            reservation.setStartDate(resultReservation.getDate("datedebut_reservation"));
            reservation.setStatut(resultReservation.getString("statut_reserve"));
            listOfReservation.add(reservation);
        }
        resultReservation.close();
        return listOfReservation;
    }

    /**
     * All methods to calculate, update, create and delete statistics
     */

    /**
     * Get the list of reservation in a period
     * @param startDate the start date of reservation
     * @param endDate the end date of reservation
     * @return a list of reservation
     * @throws SQLException
     */
    public ArrayList<Reservation> getListOfReservationForPeriod(java.util.Date startDate, java.util.Date endDate) throws SQLException {
        ArrayList<Reservation> listOfReservation;
        String query = "SELECT * FROM reserve WHERE datedebut_reservation >= ? AND datefin_reservation < ?";
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setDate(1,new Date(startDate.getTime()));
        ps.setDate(2,new Date(endDate.getTime()));
        ResultSet result = ps.executeQuery();
        listOfReservation = convertResultToListReservation(result);
        ps.close();
        result.close();
        return listOfReservation;
    }

    /**
     * methods to add a statistics
     * @param stat statistic
     * @return 1 if true
     * @throws SQLException
     */
    public int addStat(Stat stat) throws SQLException {
        int newId = (stat.getId() != 0) ? stat.getId() : getLastId("stat") + 1;
        String addQuery = " INSERT INTO stat(id_stat,datedebut_stat,datefin_stat,ca_stat,nbreserve_stat,mode_stat,id_hotel) VALUES (?,?,?,?,?,?,?)";
        PreparedStatement ps = connection.prepareStatement(addQuery);

        ps.setInt(1, newId);
        ps.setDate(2, new Date(stat.getStartDate().getTime()));
        ps.setDate(3, new Date(stat.getEndDate().getTime()));
        ps.setFloat(4, stat.getTurnover());
        ps.setInt(5, stat.getNbReservation());
        ps.setString(6, stat.getModeStat());
        ps.setInt(7,stat.getHotel().getId());

        int isAdd = ps.executeUpdate();
        ps.close();

        return isAdd;

    }

    /**
     * get the list of stat depends on the mode of the statistics
     * @param startDate the start date of stats
     * @param hotel the hotel who wants statistics
     * @param modeStat the mode of stats
     * @return a list of stats
     * @throws SQLException
     */
    public ArrayList<Stat> getStatForAPeriod(java.util.Date startDate,Hotel hotel, String modeStat) throws SQLException {
        int numberOfTurn;
        String incrementDate;
        switch (modeStat){
            case "W" :
                numberOfTurn = 1;
                incrementDate = modeStat;
                break;
            case "M" :
                numberOfTurn = 4;
                incrementDate = "W";
                break;
            case "Y" :
                numberOfTurn = 12;
                incrementDate = "M";
                break;
            default :
                numberOfTurn = 4;
                incrementDate = "W";
                break;
        }
        ArrayList<Stat> listStat = new ArrayList<>();
        java.util.Date tmpDate = new Date(startDate.getTime());
        for (int i = 0; i < numberOfTurn ;i++) {
            Stat stat = getStat(tmpDate,hotel,incrementDate);
            listStat.add(stat);
            tmpDate = StatManager.getTheRightEndDate(tmpDate,incrementDate);
        }
        return listStat;
    }

    /**
     * Get the stat
     * @param startDate the start date of the stat
     * @param hotel the hotel
     * @param modeStat the mode of stat
     * @return a stat
     * @throws SQLException
     */
    public Stat getStat(java.util.Date startDate, Hotel hotel,String modeStat) throws SQLException {
        Stat stat = new Stat();
        java.util.Date endDate = StatManager.getTheRightEndDate(startDate,modeStat);
        String query = "SELECT * FROM Stat WHERE id_hotel = ? AND datedebut_stat >= ? AND datefin_stat < ? AND mode_stat = ?";
        PreparedStatement ps = connection.prepareStatement(query);
        ps.setInt(1, hotel.getId());
        ps.setDate(2, new Date(startDate.getTime()));
        ps.setDate(3, new Date(endDate.getTime()));
        ps.setString(4, modeStat);
        ResultSet result = ps.executeQuery();
        while (result.next()) {
            stat.setId(result.getInt("id_stat"));
            stat.setStartDate(result.getDate("datedebut_stat"));
            stat.setEndDate(result.getDate("datefin_stat"));
            stat.setTurnover(result.getFloat("ca_stat"));
            stat.setNbReservation(result.getInt("nbreserve_stat"));
            stat.setModeStat(result.getString("mode_stat"));
            stat.setHotel(getHotelById(result.getInt("id_hotel")));
        }
        ps.close();
        result.close();
        return stat;

    }



}
