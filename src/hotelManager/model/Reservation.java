package hotelManager.model;

import java.util.Date;

/**
 * Reservation model
 */
public class Reservation {

    private Date startDate;
    private Date endDate;
    private Room room;
    private Customer customer;
    private int id;
    private String statut;

    public Reservation() {
    }

    public Reservation(Date startDate, Date endDate, Room room, Customer customer, int id, String statut) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.room = room;
        this.customer = customer;
        this.id = id;
        this.statut = statut;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Reservation that = (Reservation) o;

        return id == that.id &&
                (
                        startDate != null ? startDate.equals(that.startDate) : that.startDate == null &&
                                (
                                        endDate != null ? endDate.equals(that.endDate) :
                                                that.endDate == null
                                                        && room.equals(that.room)
                                                        && customer.equals(that.customer)
                                                        && (statut != null ? statut.equals(that.statut) : that.statut == null)
                                )
                );
    }

    @Override
    public int hashCode() {
        int result = startDate != null ? startDate.hashCode() : 0;
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + room.hashCode();
        result = 31 * result + customer.hashCode();
        result = 31 * result + id;
        result = 31 * result + (statut != null ? statut.hashCode() : 0);
        return result;
    }
}
