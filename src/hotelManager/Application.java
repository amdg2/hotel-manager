package hotelManager;

import hotelManager.controllers.AppController;
import hotelManager.model.DbManager;
import hotelManager.view.LoadingWindow;

/**
 * Application entry point
 */
public class Application {

    public static void main(String[] args) {
        LoadingWindow loading = new LoadingWindow();
        if (loading.connectToDatabase()) {
            loading.setVisible(false);
            AppController app = AppController.getInstance();
        } else {
            loading.setVisible(false);
            DbManager.deleteInstance();
            System.exit(-1);
        }
    }
}
