package hotelManager;

import java.util.Properties;

/**
 * Icon list
 */
public class Icons {

    private static Properties properties = PropertyLoader.getProperties();

    public static String getParametersIcon(){return properties.getProperty("icon.parameters");}
    public static String getStatsIcon(){return properties.getProperty("icon.stats");}

    public static String getBackIcon(){return properties.getProperty("icon.back");}
    public static String getBackToHotelListIcon(){return properties.getProperty("icon.back.to.hotel.list");}

    public static String getEmployeeManagerIcon(){return properties.getProperty("icon.employee.manager");}
    public static String getRoomManagerIcon(){return properties.getProperty("icon.room.manager");}
    public static String getResaManagerIcon(){return properties.getProperty("icon.resa.manager");}
    public static String getCustomerManagerIcon(){return properties.getProperty("icon.customer.manager");}

    public static String getSaveIcon(){return properties.getProperty("icon.save");}
    public static String getDeleteIcon(){return properties.getProperty("icon.delete");}
    public static String getValidateIcon(){return properties.getProperty("icon.validate");}

    public static String getPersonAddIcon(){return properties.getProperty("icon.person.add");}
    public static String getPersonAddSmallIcon(){return properties.getProperty("icon.person.add.small");}

    public static String getAddBookingIcon() {
        return properties.getProperty("icon.booking.add");
    }
}
