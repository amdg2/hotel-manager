package hotelManager.events;

/**
 * Mouse hover event interface
 */
public interface IMouseHoverEventReceiver {

    void OnMouseEnter();

    void OnMouseLeave();

    void OnMouseHover(boolean hover);
}
