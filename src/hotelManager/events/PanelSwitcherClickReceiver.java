package hotelManager.events;

import hotelManager.controllers.AppController;

/**
 * Panel switcher button click receiver
 */
public class PanelSwitcherClickReceiver implements IMouseClickEventReceiver {
    private int panelToOpen;
    private AppController app;

    public PanelSwitcherClickReceiver(AppController app, int panelToOpen) {
        this.app = app;
        this.panelToOpen = panelToOpen;
    }

    @Override
    public void onClick() {
        app.openPanel(panelToOpen);
    }

    @Override
    public void onRightClick() {

    }
}
