package hotelManager.events;

/**
 * Mouse click event listener
 */
public interface IMouseClickEventReceiver {
    /**
     * Method called on mouse left button click
     */
    void onClick();

    /**
     * Method called on mouse right button click
     */
    void onRightClick();
}
